package com.ikomet.epub;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.GridView;
import android.widget.TextView;

import com.ikomet.epub.pojo.Books;
import com.ikomet.epub.utils.NoInternet;
import com.skytree.epubtest.R;

public class BookListActivity extends Activity{

	TextView booklist_title, logout, txtwelcome;
	GridView grid_booklist;

	ProgressDialog pDialog;
	TransparentProgressDialog tpd;
	String intent_title_value;
	int intent_subcat_id;
	
	JSONArray books_list = null;
	
	ArrayList<String> books_name,bkimgurl;
	public static ArrayList<Bitmap>books_image;
	CustomGridAdapter adapter;
	public static Bitmap selected_book_image = null; 
	ArrayList<Bitmap> booksImages;
	Bitmap test;
	AlertDialog malert;
	
	public static ArrayList<Books> bookDetails;
	
	public static final String urlbookdetails = "http://storybook.verifiedwork.com/webservices/book_details.php?subcat_id=";
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_booklist);
		
		//booklist_title = (TextView)findViewById(R.id.title_grid);
		grid_booklist = (GridView)findViewById(R.id.booklist);
		logout = (TextView)findViewById(R.id.txt_logout);
		//intent_title_value = getIntent().getStringExtra("subcat_name");
		intent_subcat_id = getIntent().getIntExtra("subcat_id",-1);
		
		txtwelcome = (TextView)findViewById(R.id.txt_welcome);
		
		txtwelcome.setText("Welcome "+LoginActivity.storedUsrnm);
		
		bookDetails = new ArrayList<Books>();
		bkimgurl = new ArrayList<String>();
		books_image = new ArrayList<Bitmap>();
		//booklist_title.setText(intent_title_value);
		//test = ObjectToFileUtil.decodeBase64(CategoriesActivity.imgbs64);
		booksImages = new ArrayList<Bitmap>();
		
		DatabaseHandler dbh = new DatabaseHandler(getApplicationContext());
    	
    	if(dbh.getBooksBySubCategory(intent_subcat_id).size()>0){
		new GetBooksData().execute();
    	}else {
    		if(!CategoriesActivity.isInternetConnected(BookListActivity.this)){
    	
    			NoInternet.internetConnectionFinder(BookListActivity.this);
    		
    		}else{
    			new GetBooksData().execute();
    		}
    	}
		 /*if(CategoriesActivity.isInternetConnected (this)){
		    	
				// Calling async task to get json
		        new GetBooksData().execute();
		        
			    }else{
			    	new AlertDialog.Builder(this)
			        .setTitle("Network Check")
			        .setMessage("Internet Connection is not Enabled in your device.")
			        .setPositiveButton("Enable it", new DialogInterface.OnClickListener() {
			            public void onClick(DialogInterface dialog, int which) { 
			                // goto settings to enable it
			            	startActivity(new Intent(android.provider.Settings.ACTION_WIRELESS_SETTINGS));
			            }
			         })
			        .setNegativeButton("Exit", new DialogInterface.OnClickListener() {
			            public void onClick(DialogInterface dialog, int which) { 
			                // exit application
			            	BookListActivity.this.finish();
			            }
			         })
			        .setIcon(android.R.drawable.ic_dialog_alert)
			         .show();
			    }*/
	
		
		grid_booklist.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				// TODO Auto-generated method stub
				
				int bk_id = BookListActivity.bookDetails.get(position).getBook_id();
				selected_book_image = books_image.get(position);
				Intent intent = new Intent(BookListActivity.this, BookDetailActivity.class);
				intent.putExtra("book_id", bk_id);
				intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
				startActivity(intent);
				overridePendingTransition(R.anim.right_in, R.anim.left_out);
								
			}
		});
		
		logout.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(BookListActivity.this,
						LoginActivity.class);
				LoginActivity.prefLogin.edit().clear().commit();
				intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
				intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK
						| Intent.FLAG_ACTIVITY_CLEAR_TASK);
				startActivity(intent);
				finish();
				overridePendingTransition(R.anim.left_out, R.anim.right_in);
			}
		});
		
	}

	
	 /**
     * Async task class to get json by making HTTP call
     * */
    private class GetBooksData extends AsyncTask<Void, Void, Void> {
 
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Showing progress dialog
            tpd = new TransparentProgressDialog(BookListActivity.this, R.drawable.icai1);
            tpd.show();
 
        }
 
        @Override
        protected Void doInBackground(Void... arg0) {
            // Creating service handler class instance
//            ServiceHandler sh = new ServiceHandler();
 
            // Making a request to url and getting response
//            String jsonStr = sh.makeServiceCall(CategoriesActivity.url, ServiceHandler.GET);
 
            //Log.e("Response: ", "> " + jsonStr);
 
            /*if (CategoriesActivity.jsonDataString != null) {
                try {
                    JSONObject jsonObj = new JSONObject(CategoriesActivity.jsonDataString);
                     
                    // Getting JSON Array node
                    books_list = jsonObj.getJSONArray(CategoriesActivity.TAG_BOOKS);
                    
 
                    books_image = new ArrayList<Bitmap>();
                    books_name = new ArrayList<String>();
                    // looping through All Contacts
                    for (int i = 0; i < books_list.length(); i++) {
                        JSONObject c = books_list.getJSONObject(i);
                                  
                        String book_name = c.getString(CategoriesActivity.TAG_BOOKNAME);
                        String subcat_id = c.getString(CategoriesActivity.TAG_BOOK_SUBCATID);
                        String book_img = c.getString(CategoriesActivity.TAG_BOOKIMG);
                        
                        if(subcat_id.equals(intent_subcat_id))
                        {
                        	String urldisplay = "http://storybook.verifiedwork.com/"+book_img;
                            Bitmap mIcon11 = null;
                            try {
                                InputStream in = new java.net.URL(urldisplay).openStream();
                                mIcon11 = BitmapFactory.decodeStream(in);
                            } catch (Exception e) {
                                Log.e("Error", e.getMessage());
                                e.printStackTrace();
                            }
                        	
                        	books_name.add(book_name);
                            //books_image.add(mIcon11);
                        	books_image.add(ObjectToFileUtil.decodeBase64(book_img));
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Log.e("ServiceHandler", "Couldn't get any data from the url");
            }
 */
        	DatabaseHandler dbh = new DatabaseHandler(getApplicationContext());
        	
        	if(dbh.getBooksBySubCategory(intent_subcat_id).size()>0){
        		Log.e("Database consists of value in BookList", "Great");
        		for(int i=0;i<dbh.getBooksBySubCategory(intent_subcat_id).size();i++){
        		bookDetails.add(dbh.getBooksBySubCategory(intent_subcat_id).get(i));
        		Bitmap bitmap = BitmapFactory.decodeByteArray(dbh.getBooksBySubCategory(intent_subcat_id).get(i).getBimg(), 0, dbh.getBooksBySubCategory(intent_subcat_id).get(i).getBimg().length);
        		books_image.add(bitmap);
        		}
        		
        		
        		
        	}else{
        		Log.e("Not getting dv value in BookList", "Oops");
        		
        
        		
        	ServiceHandler sh = new ServiceHandler();
        	
        	 String jsonStr = sh.makeServiceCall(urlbookdetails+intent_subcat_id, ServiceHandler.POST);
        	
        	 Log.e("Response: ", "> " + jsonStr);
        	 
        	 if (jsonStr != null) {
                 try {
                     //JSONObject jsonObj = new JSONObject(jsonStr);
                      JSONArray jsonAr = new JSONArray(jsonStr);
                     // Getting JSON Array node
                     //books_list = jsonObj.getJSONArray(CategoriesActivity.TAG_BOOKS);
                      
                      //DatabaseHandler dbh1 = new DatabaseHandler(getApplicationContext());
                      
                      books_list = jsonAr;
  
                     books_image = new ArrayList<Bitmap>();
                     books_name = new ArrayList<String>();
                     // looping through All Contacts
                     for (int i = 0; i < books_list.length(); i++) {
                         JSONObject c = books_list.getJSONObject(i);
                         
                         int book_id = Integer.parseInt(c.getString(CategoriesActivity.TAG_BOOKID));
                         String book_name = c.getString(CategoriesActivity.TAG_BOOKNAME);
                         int book_subcat = Integer.parseInt(c.getString(CategoriesActivity.TAG_BOOKSUBCAT));
                         String book_desc = c.getString(CategoriesActivity.TAG_BOOKDESC);
                         String book_publisher = c.getString(CategoriesActivity.TAG_BOOKPUBLISHER);
                         String book_edition = c.getString(CategoriesActivity.TAG_BOOKEDITION);
                         String book_isbn = c.getString(CategoriesActivity.TAG_BOOKISBN);
                         int book_pages = Integer.parseInt(c.getString(CategoriesActivity.TAG_BOOKPAGE));
                         int book_price = Integer.parseInt(c.getString(CategoriesActivity.TAG_BOOKPRICE));
                         String imagename = c.getString(CategoriesActivity.TAG_BOOKIMG);
                         String book_img = "http://storybook.verifiedwork.com/"+c.getString(CategoriesActivity.TAG_BOOKIMG);
                         String book_pdf = "http://storybook.verifiedwork.com/"+c.getString(CategoriesActivity.TAG_BOOKPDF);
                         
                         	
                         	books_name.add(book_name);
                         	//bkimgurl.add(imagename);
                            //bkimgurl.add(book_img);
                         	
                         	DatabaseHandler dbh1 = new DatabaseHandler(getApplicationContext());
                            
                            try {
        					            				      
        				        InputStream in = new java.net.URL(book_img).openStream();
        				        Bitmap bitmap = BitmapFactory.decodeStream(in);
        				        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        				        bitmap.compress(CompressFormat.JPEG, 100, stream);
        				        byte[] image = stream.toByteArray();
        				        books_image.add(bitmap);
        				        
        				        bookDetails.add(new Books(book_id, book_subcat, book_pages, book_price, book_name, book_desc, book_publisher, book_edition, book_isbn, book_img, book_pdf, image));
        				        dbh1.addDataBooks(new Books(book_id, book_subcat, book_pages, book_price, book_name, book_desc, book_publisher, book_edition, book_isbn, book_img, book_pdf, image));
        				} catch (MalformedURLException e) {
        					// TODO Auto-generated catch block
        					e.printStackTrace();
        				} catch (IOException e) {
        					// TODO Auto-generated catch block
        					e.printStackTrace();
        				}
                            
                           /* File file;
                			FileOutputStream outputStream;
                			Bitmap bitmap;
                            
                            try {
                            	
                            	URL url = new URL(book_img);
                                HttpURLConnection connection = (HttpURLConnection) url
                                        .openConnection();
                                connection.setDoInput(true);
                                connection.connect();
                                InputStream input = connection.getInputStream();
                                bitmap = BitmapFactory.decodeStream(input);
                            	
                			    // file = File.createTempFile("MyCache", null, getCacheDir());
                			  file = new File(getDir("IkImages", Context.MODE_WORLD_READABLE), imagename);
                			 
                			    outputStream = new FileOutputStream(file);
                			    bitmap.compress(Bitmap.CompressFormat.JPEG, 90, outputStream);
                			    outputStream.flush();
                			    outputStream.close();
                			    Log.d("Write File", "Success");
                			} catch (IOException e) {
                			    e.printStackTrace();
                			}*/
                            
                            
                         }
                     }
                catch (JSONException e) {
                     e.printStackTrace();
                 }
             } else {
                 Log.e("ServiceHandler", "Couldn't get any data from the url");
             }
        	 
        	}
            return null;
        }
 
        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            // Dismiss the progress dialog
           
                tpd.dismiss();
            /**
             * Updating parsed JSON data into ListView
             * */
      
           // Log.e("Size of booklist", "   "+books_name.size());

           // adapter = new CustomGridAdapter(getApplicationContext(), books_name, books_image);
                
               
            
            adapter = new CustomGridAdapter(getApplicationContext(), bookDetails, books_image);
            
               // adapter = new CustomGridAdapter(getApplicationContext(), bookDetails);
                
            grid_booklist.setAdapter(adapter);
           
        }
 
    }


	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		
	
		
/*DatabaseHandler dbh = new DatabaseHandler(getApplicationContext());
    	
    	if(dbh.getBooksBySubCategory(intent_subcat_id).size()>0){
		new GetBooksData().execute();
    	}else {
    		if(!CategoriesActivity.isInternetConnected(BookListActivity.this)){
    	
    		
    		new AlertDialog.Builder(BookListActivity.this) .setTitle("Network Check")
			 .setMessage("Internet Connection is not Enabled in your device.").setCancelable(false)
			.setPositiveButton("Enable MobileData", new DialogInterface.OnClickListener(){
				public void onClick(DialogInterface dialog, int which) { // goto settings to enable it 
					dialog.dismiss();
					Intent intent = new Intent(android.provider.Settings.ACTION_WIRELESS_SETTINGS);
					intent.setFlags(Intent.FLAG_ACTIVITY_LAUNCHED_FROM_HISTORY);
					startActivity(intent);
				} 
				})
			 .setNegativeButton("Enable wifi", new DialogInterface.OnClickListener() {
			 public void onClick(DialogInterface dialog, int which) {
				 	dialog.dismiss();// exit application 
				 	Intent intent = new Intent(android.provider.Settings.ACTION_WIFI_SETTINGS);
				 	intent.setFlags(Intent.FLAG_ACTIVITY_LAUNCHED_FROM_HISTORY);
				 	startActivity(intent);
				 }
			 })
			.setIcon(android.R.drawable.ic_dialog_alert) .show();
    		}else{
    			
    			
    			
    			new GetBooksData().execute();
    		}
    	}
    */	
		
		
	}


	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
		
		 //malert.dismiss();
		 BookListActivity.this.finish();
		
	}

	
	
	
}
