package com.ikomet.epub;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.ikomet.epub.LocalService.LocalBinder;
import com.ikomet.epub.pojo.Books;
import com.ikomet.epub.utils.NoInternet;
import com.skytree.epub.BookInformation;
import com.skytree.epub.Setting;
import com.skytree.epubtest.R;

public class BookDetailActivity extends Activity {

	ImageView BookImage;
	TextView BookName, BookDescription, BookPublisher, BookEdition, BookISBN,
			BookPages, BookPrice, logout, txtwelcome;
	Button Download;
	TransparentProgressDialog tpd;
	JSONArray book_detail = null;
	String get_book_name;
	String bookDescription, bookPublisher, bookEdition, bookISBN, bookPages,
			bookPrice;
	public static String bookName;
	String bookUrl, bookprefix;
	File file = null;
	Boolean isSDPresent;
	Bitmap hi;
	boolean downloadstatus;

	ArrayList<Books> bookinfo;
	Bitmap bmp;

	int bookid;

	/*
	 * LocalService ls = null; Intent serviceIntent; boolean isBound = false;
	 * SkyApplication app; public static String bookPath; SkyUtility st;
	 */
	Intent serviceIntent;
	boolean isBound = false;
	LocalService ls = null;
	HomeActivity ha;
	SkyUtility st;
	SkyApplication app;
	SkyDatabase sd;

	String book_img, book_publisher;

	public static final int DIALOG_DOWNLOAD_PROGRESS = 0;

	private ProgressDialog mProgressDialog;

	DownloadFileAsync dfa = new DownloadFileAsync();

	public static String notifString = "";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_bookdetail);

		/*
		 * app = (SkyApplication)getApplication();// new doBindService();// new
		 * Setting.prepare(); // new st = new SkyUtility(this); st.makeSetup();
		 * this.registerFonts();
		 */
		// openBookViewer(bi);
		BookImage = (ImageView) findViewById(R.id.b_image);

		BookName = (TextView) findViewById(R.id.b_name);
		BookDescription = (TextView) findViewById(R.id.b_desc);
		BookPublisher = (TextView) findViewById(R.id.b_publisher);
		BookEdition = (TextView) findViewById(R.id.b_edition);
		BookISBN = (TextView) findViewById(R.id.b_isbn);
		BookPages = (TextView) findViewById(R.id.b_page);
		BookPrice = (TextView) findViewById(R.id.b_price);
		logout = (TextView) findViewById(R.id.txt_logout);
		Download = (Button) findViewById(R.id.b_download);

		ha = new HomeActivity();
		app = (SkyApplication)getApplication();
		
		get_book_name = getIntent().getStringExtra("book_name");

		bookid = getIntent().getIntExtra("book_id", -1);

		txtwelcome = (TextView) findViewById(R.id.txt_welcome);

		txtwelcome.setText("Welcome " + LoginActivity.storedUsrnm);

		bookinfo = new ArrayList<Books>();
		if (notifString.equals("")) {
			for (int i = 0; i < BookListActivity.bookDetails.size(); i++) {
				if (BookListActivity.bookDetails.get(i).getBook_id() == bookid) {
					bookName = BookListActivity.bookDetails.get(i)
							.getBook_name();
					bookUrl = BookListActivity.bookDetails.get(i).getBook_pdf();
				}
			}
		}

		
		
		LoginActivity.prefLogin = getSharedPreferences(
				LoginActivity.MyPREFERENCES, Context.MODE_PRIVATE);
		
	
		
		/*
		 * isSDPresent = android.os.Environment.getExternalStorageState()
		 * .equals(android.os.Environment.MEDIA_MOUNTED);
		 */
		new GetBooksData().execute();
		/*
		 * if(CategoriesActivity.isInternetConnected (this)){
		 * 
		 * // Calling async task to get json new GetBooksData().execute();
		 * 
		 * }else{ new AlertDialog.Builder(this) .setTitle("Network Check")
		 * .setMessage("Internet Connection is not Enabled in your device.")
		 * .setPositiveButton("Enable it", new DialogInterface.OnClickListener()
		 * { public void onClick(DialogInterface dialog, int which) { // goto
		 * settings to enable it startActivity(new
		 * Intent(android.provider.Settings.ACTION_WIRELESS_SETTINGS)); } })
		 * .setNegativeButton("Exit", new DialogInterface.OnClickListener() {
		 * public void onClick(DialogInterface dialog, int which) { // exit
		 * application BookDetailActivity.this.finish(); } })
		 * .setIcon(android.R.drawable.ic_dialog_alert) .show(); }
		 */

		Download.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				// downloadPdfContent(bookUrl);
				/*
				 * if (fileExistance(bookName)) { openPdf();
				 * 
				 * } else { if(CategoriesActivity.isInternetConnected
				 * (BookDetailActivity.this)){ startDownload(); }else{
				 * 
				 * NoInternet.internetConnectionFinder(BookDetailActivity.this);
				 * 
				 * } }
				 */if (Download.getText().toString().equals("Download")) {
					if (CategoriesActivity
							.isInternetConnected(BookDetailActivity.this)) {
						Intent intent = new Intent(BookDetailActivity.this,
								HomeActivity.class);
						if (!LoginActivity.prefLogin
								.getBoolean(bookName, false)) {
							// Toast.makeText(getApplicationContext(),
							// "Book not present going to download",
							// 1000).show();
							intent.putExtra("BOOKURL", bookUrl);
							intent.putExtra("BOOKIMGURL", book_img);
							intent.putExtra("BOOKAUTH", book_publisher);
							intent.putExtra("BOOKNAM", bookName);

						}
						intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
						startActivity(intent);
						overridePendingTransition(R.anim.left_out,
								R.anim.right_in);
					} else {
						NoInternet
								.internetConnectionFinder(BookDetailActivity.this);
					}
				} else {
					
					
					/*Toast.makeText(getApplicationContext(), "Else Part", 1000).show();
					doBindService();
					st = new SkyUtility(BookDetailActivity.this);
					st.makeSetup();
					registerFonts();
					
					// ha.makeLayout(); 
					 //ha.reload();
					 
					Setting.prepare();
					Handler handler = new Handler();
			        handler.postDelayed(new Runnable() {
						
						@Override
						public void run() {
					Log.e("ICAI",
							"Inside else of HomeActvity -> book downloaded already");
					// String destFolder = new
					// String(SkySetting.getStorageDirectory() + "/books");
					// File df = new File(destFolder);
					// df.mkdir();
					String fileName = app.sd.getFileNameByBookCode(ls
							.getBookCodeByURL(bookUrl));
					// String targetFile =
					// destFolder+"/"+app.sd.getFileNameByBookCode(ls.getBookCodeByURL(b_url));

					String coverPath = app.sd.getCoverPathByBookCode(ls
							.getBookCodeByURL(bookUrl));
					String baseDirectory = SkySetting.getStorageDirectory()
							+ "/books";
					BookInformation bi = ls.getBookInformation(fileName,
							baseDirectory, coverPath);
					
					BookInformation bi = app.bis.get(2);*/
					
					/*Intent intent = new Intent(BookDetailActivity.this, BookViewActivity.class);
					
					intent.putExtra("BOOKCODE",bi.bookCode);
					intent.putExtra("TITLE",bi.title);
					intent.putExtra("AUTHOR", bi.creator);
					intent.putExtra("BOOKNAME",bi.fileName);
					if (bi.isRTL && !bi.isRead) {
						intent.putExtra("POSITION",(double)1);
					}else {
						intent.putExtra("POSITION",bi.position);
					}		
					intent.putExtra("THEMEINDEX",app.setting.theme);
					intent.putExtra("DOUBLEPAGED",app.setting.doublePaged);		
					intent.putExtra("transitionType",app.setting.transitionType);
					intent.putExtra("GLOBALPAGINATION",app.setting.globalPagination);
					intent.putExtra("RTL",bi.isRTL);
					intent.putExtra("VERTICALWRITING",bi.isVerticalWriting);	
					
					intent.putExtra("SPREAD", bi.spread);
					intent.putExtra("ORIENTATION", bi.orientation);
					Log.e("ICAI", "final line before startActivity");
					startActivity(intent);*/
					
					/*openBookViewer(bi);
						}
					
				}, 2000);*/
			        
				}

				Intent intent = new Intent(BookDetailActivity.this,
						HomeActivity.class);
				intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
				if (!LoginActivity.prefLogin.getBoolean(bookName, false)) {
					// Toast.makeText(getApplicationContext(),
					// "Book not present going to download", 1000).show();
					intent.putExtra("BOOKURL", bookUrl);
					intent.putExtra("BOOKIMGURL", book_img);
					intent.putExtra("BOOKAUTH", book_publisher);
					intent.putExtra("BOOKNAM", bookName);

				}

				startActivity(intent);
				overridePendingTransition(R.anim.left_out, R.anim.right_in);
			}
		});

		logout.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				Intent intent = new Intent(BookDetailActivity.this,
						LoginActivity.class);
				LoginActivity.prefLogin.edit().clear().commit();
				intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
				intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK
						| Intent.FLAG_ACTIVITY_CLEAR_TASK);
				startActivity(intent);
				finish();
				overridePendingTransition(R.anim.left_out, R.anim.right_in);
			}
		});
		
		if (!LoginActivity.prefLogin.getBoolean(bookName, false)) {
			Download.setText("Download");
		} else {
			Download.setText("Read");
		}
		
		
		/*if (!Download.getText().toString().equals("Download")) {
			Log.e("ICAI", "Going to run bind service");
			doBindService();
			st = new SkyUtility(BookDetailActivity.this);
			st.makeSetup();
			ha.registerFonts();
			
			 * this.makeLayout(); this.reload();
			 
			Setting.prepare();
		}*/
	}
	public void openBookViewer(BookInformation bi) {	
    	Log.e("ICAI", "Entering openbookviewer");
		//this.hideSearchView();
		if (!bi.isDownloaded){
			Log.e("ICAI", "Going to return -> Book not yet downloaded");
			return;
		}
		Intent intent;
		if (!bi.isFixedLayout) {
			intent = new Intent(this,BookViewActivity.class);
			Log.e("ICAI", "BVA");
		}else {
			intent = new Intent(this,MagazineActivity.class);
			Log.e("ICAI", "MA");
		}
		intent.putExtra("BOOKCODE",bi.bookCode);
		intent.putExtra("TITLE",bi.title);
		intent.putExtra("AUTHOR", bi.creator);
		intent.putExtra("BOOKNAME",bi.fileName);
		if (bi.isRTL && !bi.isRead) {
			intent.putExtra("POSITION",(double)1);
		}else {
			intent.putExtra("POSITION",bi.position);
		}		
		intent.putExtra("THEMEINDEX",app.setting.theme);
		intent.putExtra("DOUBLEPAGED",app.setting.doublePaged);		
		intent.putExtra("transitionType",app.setting.transitionType);
		intent.putExtra("GLOBALPAGINATION",app.setting.globalPagination);
		intent.putExtra("RTL",bi.isRTL);
		intent.putExtra("VERTICALWRITING",bi.isVerticalWriting);	
		
		intent.putExtra("SPREAD", bi.spread);
		intent.putExtra("ORIENTATION", bi.orientation);
		Log.e("ICAI", "final line before startActivity");
		startActivity(intent);
		
	}
	public void registerFonts() {
		this.registerCustomFont("Underwood","uwch.ttf");
		this.registerCustomFont("Mayflower","Mayflower Antique.ttf");		
	}
	
	public void registerCustomFont(String fontFaceName,String fontFileName) {
		st.copyFontToDevice(fontFileName);
		app.customFonts.add(new CustomFont(fontFaceName,fontFileName));
	}
	
	private ServiceConnection mConnection = new ServiceConnection() {
		/* 두번째 파라미터 service는 Service의 onBind()가 리턴한 LocalBinder 인스턴스이다 */
		public void onServiceConnected(ComponentName className, IBinder service) {
			LocalBinder binder = (LocalBinder) service;
			ls = binder.getService();
			isBound = true;
		}

		public void onServiceDisconnected(ComponentName arg0) {
			isBound = false;
		}
	};

	void doBindService() {
		Intent intent = new Intent(this, LocalService.class);
		bindService(intent, mConnection, Context.BIND_AUTO_CREATE);
		isBound = true;
	}

	void doUnbindService() {
		if (isBound) {
			unbindService(mConnection);
			isBound = false;
		}
	}

	@Override
	protected void onDestroy() {
		
		super.onDestroy();
		//this.unbindService(mConnection);
	}
	/**
	 * Async task class to get json by making HTTP call
	 * */
	private class GetBooksData extends AsyncTask<Void, Void, Void> {

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			// Showing progress dialog
			tpd = new TransparentProgressDialog(BookDetailActivity.this,
					R.drawable.icai1);
			tpd.show();

		}

		@Override
		protected Void doInBackground(Void... arg0) {
			// Creating service handler class instance
			// ServiceHandler sh = new ServiceHandler();

			// Making a request to url and getting response
			/*
			 * String jsonStr = sh.makeServiceCall(CategoriesActivity.url,
			 * ServiceHandler.GET);
			 */
			// Log.e("Response: ", "> " + jsonStr);

			/*
			 * if (CategoriesActivity.jsonDataString != null) { try { JSONObject
			 * jsonObj = new JSONObject(CategoriesActivity.jsonDataString);
			 * 
			 * // Getting JSON Array node book_detail = jsonObj
			 * .getJSONArray(CategoriesActivity.TAG_BOOKS);
			 * 
			 * // looping through All Contacts for (int i = 0; i <
			 * book_detail.length(); i++) { JSONObject c =
			 * book_detail.getJSONObject(i);
			 * 
			 * String book_name = c .getString(CategoriesActivity.TAG_BOOKNAME);
			 * String book_desc = c .getString(CategoriesActivity.TAG_BOOKDESC);
			 * String book_publisher = c
			 * .getString(CategoriesActivity.TAG_BOOKPUBLISHER); String
			 * book_edition = c .getString(CategoriesActivity.TAG_BOOKEDITION);
			 * String book_isbn = c .getString(CategoriesActivity.TAG_BOOKISBN);
			 * String book_pages = c
			 * .getString(CategoriesActivity.TAG_BOOKPAGE); String book_price =
			 * c .getString(CategoriesActivity.TAG_BOOKPRICE);
			 * 
			 * 
			 * 
			 * 
			 * // String book_img = //
			 * c.getString(CategoriesActivity.TAG_BOOKIMG);
			 * Log.e("Book Name is .........", book_name); if
			 * (book_name.equals(get_book_name)) {
			 * 
			 * bookName = book_name; bookDescription = book_desc; bookPublisher
			 * = book_publisher; bookEdition = book_edition; bookISBN =
			 * book_isbn; bookPages = book_pages; bookPrice = book_price;
			 * bookprefix = c .getString(CategoriesActivity.TAG_BOOKPDF);
			 * bookUrl = "http://storybook.verifiedwork.com/" + bookprefix;
			 * Log.e("Name of the book is...", book_name + " URL" + bookUrl); }
			 * else { Log.e("***Book Name*****", book_name); } } } catch
			 * (JSONException e) { e.printStackTrace(); } } else {
			 * Log.e("ServiceHandler", "Couldn't get any data from the url"); }
			 */

			if (!BookDetailActivity.notifString.equals("")) {
				try {

					/*
					 * JSONObject jsonObj = new
					 * JSONObject(BookDetailActivity.notifString); JSONArray
					 * jsonAr;
					 * 
					 * jsonAr = jsonObj.getJSONArray("web_data");
					 */
					JSONObject c = new JSONObject(
							BookDetailActivity.notifString);
					for (int i = 0; i < c.length(); i++) {
						// JSONObject c = jsonAr.getJSONObject(i);

						int book_id = Integer.parseInt(c
								.getString(CategoriesActivity.TAG_BOOKID));
						String book_name = c
								.getString(CategoriesActivity.TAG_BOOKNAME);
						bookName = book_name;

						int book_subcat = Integer.parseInt(c
								.getString(CategoriesActivity.TAG_BOOKSUBCAT));
						String book_desc = c
								.getString(CategoriesActivity.TAG_BOOKDESC);
						book_publisher = c
								.getString(CategoriesActivity.TAG_BOOKPUBLISHER);
						String book_edition = c
								.getString(CategoriesActivity.TAG_BOOKEDITION);
						String book_isbn = c
								.getString(CategoriesActivity.TAG_BOOKISBN);
						int book_pages = Integer.parseInt(c
								.getString(CategoriesActivity.TAG_BOOKPAGE));
						int book_price = Integer.parseInt(c
								.getString(CategoriesActivity.TAG_BOOKPRICE));
						String imagename = c
								.getString(CategoriesActivity.TAG_BOOKIMG);
						book_img = "http://storybook.verifiedwork.com/"
								+ c.getString(CategoriesActivity.TAG_BOOKIMG);
						String book_pdf = "http://storybook.verifiedwork.com/"
								+ c.getString(CategoriesActivity.TAG_BOOKPDF);
						bookUrl = book_pdf;

						// bkimgurl.add(imagename);
						// bkimgurl.add(book_img);

						try {

							InputStream in = new java.net.URL(book_img)
									.openStream();
							bmp = BitmapFactory.decodeStream(in);
							ByteArrayOutputStream stream = new ByteArrayOutputStream();
							bmp.compress(CompressFormat.PNG, 0, stream);
							byte[] image = stream.toByteArray();

							bookinfo.add(new Books(book_id, book_subcat,
									book_pages, book_price, book_name,
									book_desc, book_publisher, book_edition,
									book_isbn, book_img, book_pdf, image));

						} catch (MalformedURLException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}

					}

				} catch (JSONException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				// Getting JSON Array node
				// books_list =
				// jsonObj.getJSONArray(CategoriesActivity.TAG_BOOKS);

				// looping through All Contacts

			}

			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
			// Dismiss the progress dialog
			if (tpd.isShowing())
				tpd.dismiss();

			/*
			 * if (fileExistance(bookName)) { Download.setText("Read");
			 * Log.e("PDF is present", "Open pdf"); } else {
			 * Download.setText("Download"); }
			 */
			if (!LoginActivity.prefLogin.getBoolean(bookName, false)) {
				Download.setText("Download");
			} else {
				Download.setText("Read");
			}
			// LoginActivity.prefBookCheck =
			// getSharedPreferences(LoginActivity.MyPREFERENCES,
			// Context.MODE_PRIVATE);
			if (!BookDetailActivity.notifString.equals("")) {

				DatabaseHandler dbh = new DatabaseHandler(
						getApplicationContext());

				for (int i = 0; i < bookinfo.size(); i++) {
					BookImage.setImageBitmap(bmp);
					BookName.setText("Book Name : "
							+ bookinfo.get(i).getBook_name());
					BookDescription.setText("Description : "
							+ bookinfo.get(i).getBook_desc());
					// BookPublisher.setText("Publisher : " + bookPublisher);
					BookPublisher.setText("Author : "
							+ bookinfo.get(i).getBook_publisher());
					BookEdition.setText("Edition : "
							+ bookinfo.get(i).getBook_edition());
					BookISBN.setText("ISBN : " + bookinfo.get(i).getBook_isbn());
					BookPages.setText("Number of Pages : "
							+ bookinfo.get(i).getBook_pages());
					// BookPrice.setText("Price : Rs."+bookPrice);
					BookPrice.setText("Price : Free");
					book_img = bookinfo.get(i).getBook_image();
					book_publisher = bookinfo.get(i).getBook_publisher();
					/*
					 * dbh.addDataBooks(new Books(bookinfo.get(i).getBook_id(),
					 * bookinfo.get(i).getBook_subcat_id(),
					 * bookinfo.get(i).getBook_pages(),
					 * bookinfo.get(i).getBook_price(),
					 * bookinfo.get(i).getBook_name(),
					 * bookinfo.get(i).getBook_desc(),
					 * bookinfo.get(i).getBook_publisher(),
					 * bookinfo.get(i).getBook_edition(),
					 * bookinfo.get(i).getBook_isbn(),
					 * bookinfo.get(i).getBook_image(),
					 * bookinfo.get(i).getBook_pdf(), bimg));
					 */

				}

			} else {
				for (int i = 0; i < BookListActivity.bookDetails.size(); i++) {
					if (BookListActivity.bookDetails.get(i).getBook_id() == bookid) {
						// BookImage.setImageBitmap(BookListActivity.selected_book_image);
						BookImage
								.setImageBitmap(BookListActivity.selected_book_image);
						BookName.setText("Book Name : "
								+ BookListActivity.bookDetails.get(i)
										.getBook_name());
						BookDescription.setText("Description : "
								+ BookListActivity.bookDetails.get(i)
										.getBook_desc());
						// BookPublisher.setText("Publisher : " +
						// bookPublisher);
						BookPublisher.setText("Author : "
								+ BookListActivity.bookDetails.get(i)
										.getBook_publisher());
						BookEdition.setText("Edition : "
								+ BookListActivity.bookDetails.get(i)
										.getBook_edition());
						BookISBN.setText("ISBN : "
								+ BookListActivity.bookDetails.get(i)
										.getBook_isbn());
						BookPages.setText("Number of Pages : "
								+ BookListActivity.bookDetails.get(i)
										.getBook_pages());
						// BookPrice.setText("Price : Rs."+bookPrice);
						BookPrice.setText("Price : Free");

						book_img = BookListActivity.bookDetails.get(i)
								.getBook_image();
						book_publisher = BookListActivity.bookDetails.get(i)
								.getBook_publisher();

					}
				}
			}

			/*
			 * if (fileExistance(bookName)){ Download.setText("Read"); }else{
			 * Download.setText("Download"); }
			 */

			if (!LoginActivity.prefLogin.getBoolean(bookName, false)) {
				Download.setText("Download");
			} else {
				Download.setText("Read");
			}

		}

	}

	public boolean fileExistance(String fname) {
		File file = getBaseContext().getFileStreamPath(fname);
		// bookPath = "data/data/com.skytree.epubtest/"+bookName;
		Log.e("File", "" + file);
		return file.exists();
	}

	/*
	 * @Override protected void onDestroy() {
	 * 
	 * super.onDestroy(); this.unbindService(mConnection); }
	 * 
	 * private ServiceConnection mConnection = new ServiceConnection() { 두번째
	 * 파라미터 service는 Service의 onBind()가 리턴한 LocalBinder 인스턴스이다 public void
	 * onServiceConnected(ComponentName className,IBinder service) { LocalBinder
	 * binder = (LocalBinder) service; ls = binder.getService(); isBound = true;
	 * }
	 * 
	 * public void onServiceDisconnected(ComponentName arg0) { isBound = false;
	 * } }; public void registerFonts() {
	 * this.registerCustomFont("Underwood","uwch.ttf");
	 * this.registerCustomFont("Mayflower","Mayflower Antique.ttf"); }
	 * 
	 * void doBindService() { Intent intent = new Intent(this,
	 * LocalService.class); bindService(intent, mConnection,
	 * Context.BIND_AUTO_CREATE); isBound = true; }
	 * 
	 * void doUnbindService() { if (isBound) { unbindService(mConnection);
	 * isBound = false; } }
	 * 
	 * public void registerCustomFont(String fontFaceName,String fontFileName) {
	 * st.copyFontToDevice(fontFileName); app.customFonts.add(new
	 * CustomFont(fontFaceName,fontFileName)); }
	 * 
	 * private void openPdf() {
	 * 
	 * ls.installBook(bookPath);
	 * 
	 * Intent intent = new Intent(this,BookViewActivity.class);
	 * 
	 * intent.putExtra("BOOKCODE",0); intent.putExtra("TITLE","ICAI");
	 * intent.putExtra("AUTHOR", "ICAI"); intent.putExtra("BOOKNAME",bookName);
	 * 
	 * intent.putExtra("THEMEINDEX",app.setting.theme);
	 * intent.putExtra("DOUBLEPAGED",app.setting.doublePaged);
	 * intent.putExtra("transitionType",app.setting.transitionType);
	 * intent.putExtra("GLOBALPAGINATION",app.setting.globalPagination);
	 * intent.putExtra("RTL",false); intent.putExtra("VERTICALWRITING",false);
	 * 
	 * intent.putExtra("SPREAD", 0); intent.putExtra("ORIENTATION", 0);
	 * 
	 * startActivity(intent);
	 * 
	 * 
	 * 
	 * File file = getBaseContext().getFileStreamPath(bookName); if
	 * (file.exists()) { Uri path = Uri.fromFile(file); Intent intent = new
	 * Intent(Intent.ACTION_VIEW); intent.setDataAndType(path,
	 * "application/pdf"); intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
	 * 
	 * //Download.setText("Read"); startActivity(intent);
	 * 
	 * 
	 * }
	 * 
	 * Intent intent = new Intent(BookDetailActivity.this,
	 * ReaderWithControllerActivity.class); startActivity(intent);
	 * 
	 * 
	 * }
	 */

	/*
	 * public int getPositionByBookCode(int bookCode) { int position = -1; for
	 * (int i=0; i<app.bis.size(); i++) { BookInformation bi = app.bis.get(i);
	 * if (bi.bookCode==bookCode) { position = i; break; } } return position; }
	 */

	private void startDownload() {
		String url = bookUrl;
		dfa.execute(url);
	}

	@Override
	protected Dialog onCreateDialog(int id) {
		switch (id) {
		case DIALOG_DOWNLOAD_PROGRESS:
			mProgressDialog = new ProgressDialog(this);
			mProgressDialog.setMessage("Downloading file..");
			mProgressDialog.setCancelable(false);
			mProgressDialog.setButton(DialogInterface.BUTTON_NEGATIVE,
					"Cancel", new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int which) {

							dfa.cancel(true);
							dismissDialog(DIALOG_DOWNLOAD_PROGRESS);
							dialog.cancel();
							BookDetailActivity.this.finish();
						}
					});
			mProgressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
			mProgressDialog.setCancelable(false);
			mProgressDialog.show();
			return mProgressDialog;
		default:
			return null;
		}
	}

	class DownloadFileAsync extends AsyncTask<String, String, String> {

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			downloadstatus = true;
			showDialog(DIALOG_DOWNLOAD_PROGRESS);
			Log.e("Started download", "STD DOWNL");
		}

		@Override
		protected String doInBackground(String... aurl) {
			int count;

			try {
				URL url = new URL(aurl[0]);
				URLConnection conexion = url.openConnection();
				conexion.connect();

				int lenghtOfFile = conexion.getContentLength();
				Log.d("ANDRO_ASYNC", "Lenght of file: " + lenghtOfFile);

				InputStream input = new BufferedInputStream(url.openStream());

				OutputStream output = openFileOutput(bookName,
						Context.MODE_PRIVATE);

				byte data[] = new byte[1024];

				long total = 0;

				if (!dfa.isCancelled()) {
					while (!dfa.isCancelled()
							&& (count = input.read(data)) != -1) {
						total += count;
						publishProgress(""
								+ (int) ((total * 100) / lenghtOfFile));
						output.write(data, 0, count);

					}

					output.flush();
					output.close();
					input.close();

				} /*
				 * else{
				 * 
				 * File file = getBaseContext().getFileStreamPath(bookName);
				 * file.delete();
				 * 
				 * }
				 */

			} catch (Exception e) {
				e.printStackTrace();
			} finally {
				if (dfa.isCancelled()) {
					File file = getBaseContext().getFileStreamPath(bookName);
					file.delete();
					dfa.cancel(false);
				}
			}

			return null;

		}

		protected void onProgressUpdate(String... progress) {
			Log.d("ANDRO_ASYNC", progress[0]);
			mProgressDialog.setProgress(Integer.parseInt(progress[0]));
		}

		@Override
		protected void onPostExecute(String unused) {
			dismissDialog(DIALOG_DOWNLOAD_PROGRESS);
			// Download.setText("Read");
			if (dfa.isCancelled()) {
				Log.e("Entered if statement", "Entered");
				File file = getBaseContext().getFileStreamPath(bookName);
				file.delete();
				Log.e("Leaved if statement", "Leaved");
			}
			if (fileExistance(bookName)) {
				Download.setText("Read");
				// openPdf();

			} else {
				Toast.makeText(getApplicationContext(), "File doesn't exist",
						Toast.LENGTH_SHORT).show();

			}
		}

	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		if (!LoginActivity.prefLogin.getBoolean(bookName, false)) {
			Download.setText("Download");
		}  {
			Download.setText("Read");
		}
		/*
		 * if (fileExistance(bookName)) { openPdf();
		 * 
		 * } else { startDownload();
		 * 
		 * }
		 */
	}

}
