package com.ikomet.epub;

import java.io.IOException;
import java.nio.charset.UnmappableCharacterException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.ikomet.epub.utils.NoInternet;
import com.skytree.epubtest.R;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

public class RegisterActivity extends Activity{

	EditText fname,uname,pwd,cnfmpwd,mail,city,phone;
	RadioGroup rggender;
	RadioButton rdmale,rdfemale;
	Button register;
	JSONArray ja=null;
	JSONObject jo=null;
	 // Progress Dialog
    private ProgressDialog pDialog;
    
    String gender;
    
    //GoogleCloudMessaging gcm;
	Context context;
	String regId, gcmid;

	public static final String REG_ID = "regId";
	private static final String APP_VERSION = "appVersion";
	static final String TAG = "Register Activity";
	String gcm1;
    // JSON parser class
    JSONParser jsonParser = new JSONParser();

    //php login script

    //localhost :
    //testing on your device
    //put your local ip instead,  on windows, run CMD > ipconfig
    //or in mac's terminal type ifconfig and look for the ip under en0 or en1
   // private static final String LOGIN_URL = "http://xxx.xxx.x.x:1234/webservice/register.php";

    //testing on Emulator:
    //private static final String LOGIN_URL = "http://10.0.2.2:1234/webservice/register.php";

  //testing from a real server:
    private static final String REG_URL = "http://storybook.verifiedwork.com/webservices/user_registration.php";

    //ids
    private static final String TAG_SUCCESS = "code";
    private static final String TAG_MESSAGE = "errors";
    private static final String TAG_USER = "users";
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_register);
		
		uname = (EditText)findViewById(R.id.ed_usrnm);
		mail = (EditText)findViewById(R.id.ed_mailid);
		//uname = (EditText)findViewById(R.id.ed_username);
		phone = (EditText)findViewById(R.id.ed_phone);
		rggender = (RadioGroup)findViewById(R.id.rg_gender);
		rdmale = (RadioButton)findViewById(R.id.rd_male);
		rdfemale = (RadioButton)findViewById(R.id.rd_female);
		pwd = (EditText)findViewById(R.id.ed_pass);
		pwd.setTypeface(Typeface.DEFAULT);
		cnfmpwd = (EditText)findViewById(R.id.ed_cnfmpass);
		cnfmpwd.setTypeface(Typeface.DEFAULT);
		//rdmale.setChecked(true);
		/*rdmale.setText("Male");
		rdmale.setText("Female");*/
		register = (Button)findViewById(R.id.bt_register);
	
		uname.setOnFocusChangeListener(new OnFocusChangeListener() {

			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				if (!hasFocus) {
					
					if (uname.getText().length() >=6) {
						uname.setError(null);
					} else if(uname.getText().length() >=15){
						uname.setError("Username should not exceed 15 characters");
					}else{
						uname.setError("Username should be minimum 6 characters");
					}
				}
			}
		});
		
		phone.setOnFocusChangeListener(new OnFocusChangeListener() {

			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				if (!hasFocus) {
					
					if (phone.getText().length() <=10) {
						phone.setError(null);
					} else{
						phone.setError("Mobile number must be minimum 10 digits");
					}
				}
			}
		});
		
		mail.setOnFocusChangeListener(new OnFocusChangeListener() {

			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				if (!hasFocus) {
					String regex = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
					Pattern pattern = Pattern.compile(regex);
					Matcher matcher = pattern.matcher(mail.getText()
							.toString());
					if (mail.getText().length() >= 4 && matcher.matches()) {
						mail.setError(null);
					} else {
						mail.setError("Invalid Email id");
						/*Toast.makeText(getApplicationContext(),
								"Invalid Email id", 1000).show();*/
					}
				}
			}
		});
		
		pwd.setOnFocusChangeListener(new OnFocusChangeListener() {

			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				if (!hasFocus) {
					
					if (pwd.getText().length()>6) {
						pwd.setError(null);
					} else if(pwd.getText().length()>15) {
						pwd.setError("Password should be maximum 15 characters");
					}else{
						pwd.setError("Password should be minimum 6 characters");
					}
				}
			}
		});
		
		cnfmpwd.setOnFocusChangeListener(new OnFocusChangeListener() {

			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				if (!hasFocus) {
					
					if (pwd.getText().toString().equals(cnfmpwd.getText().toString())) {
						cnfmpwd.setError(null);
					}else{
						cnfmpwd.setError("Password doesn't match");
					}
				}
			}
		});
		
		
		//uname.requestFocus();
		
		/*pwd.addTextChangedListener(new TextWatcher() {
			
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				// TODO Auto-generated method stub
				if(s.length()==0){
					pwd.setError("Please fill the password");
				}
				if(s.length()<6){
					pwd.setError("Password should be minimum 6 characters");
				}else if(s.length()>15){
					pwd.setError("Password is limited to 15 characters");
				}
			}
			
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub
				if(s.length()==0){
					pwd.setError("Please fill the password");
				}
				
			}
			
			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub
				passForVal = s.toString();
			}
		});*/
		
		/*cnfmpwd.addTextChangedListener(new TextWatcher() {
			
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				// TODO Auto-generated method stub
				if(s.equals("")){
					cnfmpwd.setError("Please fill the to confirm password");
				}
				if(!s.equals(passForVal)){
					cnfmpwd.setError("Password doesn't match");
				}
			}
			
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub
				if(s.equals("")){
					cnfmpwd.setError("Please fill the to confirm password");
				}
			}
			
			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub
				if(!s.equals(passForVal)){
					cnfmpwd.setError("Password doesn't match");
				}
			}
		});*/
		
		/*phone.addTextChangedListener(new TextWatcher() {
			
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				// TODO Auto-generated method stub
				if(s.length()==0){
					cnfmpwd.setError("Please fill the phone number");
				}
			}
			
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub
				if(s.length()==0){
					cnfmpwd.setError("Please fill the phone number");
				}
			}
			
			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub
				
			}
		});*/
		
		register.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(registrationvalidation()){
					
				if(CategoriesActivity.isInternetConnected(RegisterActivity.this)){	
					new CreateUser().execute();
				}else{
					
					NoInternet.internetConnectionFinder(RegisterActivity.this);
				}
				}else{
					
				}
				}
		});
		
		
		
	}
	
	

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		
	}

	class CreateUser extends AsyncTask<String, String, String> {

		 /**
        * Before starting background thread Show Progress Dialog
        * */
		boolean failure = false;

       @Override
       protected void onPreExecute() {
           super.onPreExecute();
           pDialog = new ProgressDialog(RegisterActivity.this);
           pDialog.setMessage("Registering...");
           pDialog.setIndeterminate(false);
           pDialog.setCancelable(false);
           pDialog.show();
       }

		@Override
		protected String doInBackground(String... args) {
			// TODO Auto-generated method stub
			 // Check for success tag
           int code;
           //String firstname = fname.getText().toString();
           String username = uname.getText().toString();
           String gendername;
           if(rdmale.isChecked()==true){
        	   gendername = rdmale.getText().toString();
           }else{
        	   gendername = rdfemale.getText().toString();
           }
            
           String password = pwd.getText().toString();
           //String cnfmpassword = cnfmpwd.getText().toString();
           String mailid = mail.getText().toString();
           //String cityname = city.getText().toString();
           String mobile = phone.getText().toString();
           
           /*gcm = GoogleCloudMessaging.getInstance(RegisterActivity.this);
           if (gcm == null) {
				gcm = GoogleCloudMessaging.getInstance(context);
			}
           String gcmRegId="";*/
		/*try {
			//gcmRegId = gcm.register(Config.GOOGLE_PROJECT_ID);
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}*/
           
           
           
           //String gcmRegId = gcmid;
          // Log.e("GCM", "Registration Id = "+ gcmRegId);
          // showToast("Registration Id = "+ gcmRegId);
           try {
               // Building Parameters
        	  // fnm,gender,pwd,mail,contact

               List<NameValuePair> params = new ArrayList<NameValuePair>();
               params.add(new BasicNameValuePair("fnm", username));
               //params.add(new BasicNameValuePair("unm", username));
               params.add(new BasicNameValuePair("gender", gendername));
               params.add(new BasicNameValuePair("pwd", password));
               //params.add(new BasicNameValuePair("cpwd", cnfmpassword));
               params.add(new BasicNameValuePair("mail", mailid));
               params.add(new BasicNameValuePair("contact", mobile));
              // params.add(new BasicNameValuePair("reg_id", gcmRegId));
               
               Log.e("request!", "starting");

               /*//Posting user data to script
               JSONObject json = jsonParser.makeHttpRequest(
                      LOGIN_URL, "POST", params);*/
           
               ServiceHandler sh = new ServiceHandler();
               sh.makeServiceCall(REG_URL, 2, params);
             //Posting user data to script
               
               String jsonStr = sh.makeServiceCall(REG_URL, 2, params);
               
               JSONObject json = new JSONObject(jsonStr);
               
               /*JSONObject json = jsonParser.makeHttpRequest(
                      REG_URL, "POST", params);*/
               //Log.e("Login attempt", json);
               // full json response
               Log.e("Login attempt", json.toString());

               /*jo = new JSONObject(json);
               ja = jo.getJSONArray("errors");*/
               
               // json success element
               code = Integer.parseInt(json.getString("code"));
               if (code == 200) {
               	Log.e("User Created!", json.toString());
               	
               	LoginActivity.prefLogin=getSharedPreferences(LoginActivity.MyPREFERENCES,Context.MODE_PRIVATE);
	           	 Editor editor = LoginActivity.prefLogin.edit();
	           	 editor.putString("UNAME", username);
	             editor.commit();
	            LoginActivity.storedUsrnm = LoginActivity.prefLogin.getString("UNAME", "");                	
               	RegisterActivity.this.finish();
               	overridePendingTransition(R.anim.right_in, R.anim.left_out);
               	return json.getString(TAG_MESSAGE);
               }else{
               	Log.e("Login Failure!", json.getString(TAG_MESSAGE));
               	return null;

               }
           } catch (JSONException e) {
               e.printStackTrace();
           }

           return null;

		}
		/**
        * After completing background task Dismiss the progress dialog
        * **/
       protected void onPostExecute(String file_url) {
           // dismiss the dialog once product deleted
           pDialog.dismiss();
           Intent intent = new Intent(RegisterActivity.this, CategoriesActivity.class);
           startActivity(intent);
           finish();
           if (file_url != null){
           	Toast.makeText(RegisterActivity.this, file_url, Toast.LENGTH_LONG).show();
           }
           
           /*LoginActivity.prefLogin = getSharedPreferences(LoginActivity.MyPREFERENCES, Context.MODE_PRIVATE);
           if(!LoginActivity.prefLogin.getString("UNAME", "").equalsIgnoreCase("")){
        	   
           }*/
       }

	}
		
	public boolean registrationvalidation()
	{
		
		String username = uname.getText().toString().trim();
		//String gender;
		
		String password=pwd.getText().toString().trim();
		String cpwd=cnfmpwd.toString();
		String mail_id=mail.getText().toString().trim();
		String mobile_no = phone.getText().toString().trim();
		//String gcmregid = ;
		
		
		if (uname.getText().toString().trim().equalsIgnoreCase("") || mail.getText().toString().trim().equalsIgnoreCase("")
				|| pwd.getText().toString().trim().equalsIgnoreCase("")
				|| cnfmpwd.getText().toString().trim().equalsIgnoreCase("")
				|| phone.getText().toString().trim().equalsIgnoreCase("") 
				|| !(pwd.getText().toString().trim().equals(cnfmpwd.getText().toString().trim())))
		{
						
			if(uname.getText().toString().trim().equalsIgnoreCase(""))
			{
				showToast("Invalid username");
				
			}
			if(pwd.getText().toString().trim().equalsIgnoreCase(""))
			{
				showToast("Invalid password");
				
			}
			if(cnfmpwd.getText().toString().trim().equalsIgnoreCase(""))
			{
				showToast("Invalid password");
				
			}
			if(mail.getText().toString().trim().equalsIgnoreCase(""))
			{
				showToast("Invalid e-mail");
				
			}
			if(phone.getText().toString().trim().equalsIgnoreCase(""))
			{
				showToast("Invalid phone number");
				
			}
			if(!(pwd.getText().toString().trim().equals(cnfmpwd.getText().toString().trim())))
			{
				showToast("Password Mismatch");
			}
			
			showToast("Please Fill all the fields");
			return false;
		}
		else
		{
			
			/*if (pwd.length() > 6)
			{
				if (pwd.length() > 6)
					return true;
				else
				{
					showToast("Password Should be Minimum 6 Digits");
					pwd.setText("");		
					pwd.setError("Password Should be Minimum 6 Digits");
					pwd.requestFocus();
					return false;
				}
			}				
			{
				showToast("Invalid Mobile Number");
				//mobile_no.setText("");
				//mobile_no.setError("Invalid Mobile Number",dr);
				//mobile_no.requestFocus();
				return false;
			}*/
			return true;
		}
		
		
	}
	private void showToast(String msg)
	{
		Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_SHORT).show();
	}
	
	
	
}
