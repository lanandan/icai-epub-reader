package com.ikomet.epub;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

import com.ikomet.epub.pojo.Books;
import com.skytree.epubtest.R;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.AsyncTask;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
 
public class CustomGridAdapter extends BaseAdapter{
      private Context mContext;
     // private final ArrayList<String> b_name;
      //private final ArrayList<Bitmap> b_img;
      private final ArrayList<Books> b_img;
      private final ArrayList<Bitmap> bimp;
      Bitmap bitmap=null;
 
       /* public CustomGridAdapter(Context c,ArrayList<String> b_name,ArrayList<Bitmap> b_img ) {
            mContext = c;
            this.b_name = b_name;
            this.b_img = b_img;
        }*/
      
      public CustomGridAdapter(Context c, ArrayList<Books> b_img, ArrayList<Bitmap> bimp ) {
          mContext = c;
         this.bimp = bimp;
          this.b_img = b_img;
      }
 
        @Override
        public int getCount() {
            // TODO Auto-generated method stub
            return b_img.size();
        }
 
        @Override
        public Object getItem(int position) {
            // TODO Auto-generated method stub
            return position;
        }
 
        @Override
        public long getItemId(int position) {
            // TODO Auto-generated method stub
            return position;
        }
 
        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            // TODO Auto-generated method stub
            //View grid;
            LayoutInflater inflater = (LayoutInflater) mContext
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
 
            if (convertView == null) {
 
                convertView = inflater.inflate(R.layout.booklist_grid_single, parent, false);
                
            }
            	//RelativeLayout relt = (RelativeLayout)convertView.findViewById(R.id.rel_grid);
            	TextView textView = (TextView) convertView.findViewById(R.id.grid_text);
                ImageView imageView = (ImageView)convertView.findViewById(R.id.grid_image);
            
                
/*                Log.e("BOOK NAME", "  "+b_name.get(position));
                textView.setText(b_name.get(position));*/
                textView.setText(b_img.get(position).getBook_name());
                textView.setGravity(Gravity.CENTER);
                //textView.setTextColor(mContext.getResources().getColor(R.color.colorPrimary));
                textView.setVisibility(View.VISIBLE);
               /* relt.setLayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);*/
                
                /*try {
				      //new ABC().execute(b_img.get(position));
			        InputStream in = new java.net.URL(b_img.get(position)).openStream();
			        bitmap = BitmapFactory.decodeStream(in);
			        			        
			} catch (MalformedURLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}*/
                imageView.setImageBitmap(bimp.get(position));
                //imageView.setImageBitmap(BitmapFactory.decodeByteArray(b_img.get(position).getBimg(), 0, b_img.get(position).getBimg().length));
                
                return convertView;
        }

        /*class ABC extends AsyncTask<String, Void, Void>{

			@Override
			protected Void doInBackground(String... params) {
				// TODO Auto-generated method stub
				
				String url = params[0];
				//Bitmap bitmap=null;
                try {
				      
			        InputStream in = new java.net.URL(url).openStream();
			        bitmap = BitmapFactory.decodeStream(in);
			        			        
			} catch (MalformedURLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
                
                
                
				return null;
			}

			@Override
			protected void onPostExecute(Void result) {
				// TODO Auto-generated method stub
				super.onPostExecute(result);
				
				
			}
			
			
        	
        }*/
        
}