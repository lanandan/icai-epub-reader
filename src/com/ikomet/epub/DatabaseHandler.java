package com.ikomet.epub;

import java.util.ArrayList;
import java.util.List;

import com.ikomet.epub.pojo.Books;
import com.ikomet.epub.pojo.Category;
import com.ikomet.epub.pojo.Subcategory;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class DatabaseHandler extends SQLiteOpenHelper {

	// All Static variables
	// Database Version
	private static final int DATABASE_VERSION = 1;

	// Database Name
	private static final String DATABASE_NAME = "booksManager";

	// Contacts table name
	private static final String TABLE_BOOKS = "books";
	
	public static final String KEY_BOOKID = "b_id";
	public static final String KEY_BOOKNAME = "b_nm";
	public static final String KEY_BOOKSUBCAT = "b_subcat";
	public static final String KEY_BOOKDESC = "b_desc";
	public static final String KEY_BOOKPUBLISHER = "b_publisher";
	public static final String KEY_BOOKEDITION = "b_edition";
	public static final String KEY_BOOKISBN = "b_isbn";
	public static final String KEY_BOOKPAGE = "b_page";
	public static final String KEY_BOOKPRICE = "b_price";
	public static final String KEY_BOOKIMG = "b_img";
	public static final String KEY_BOOKPDF = "b_pdf";
	public static final String KEY_BOOK_SUBCATID = "subcat_id";
	public static final String KEY_BOOK_PARENTID = "parent_id";
	public static final String KEY_BOOK_SUBCATNAME = "subcat_nm";

	// Categories
	public static final String TABLE_CATEGORY = "category";

	public static final String KEY_CAT_ID = "cat_id";
	public static final String KEY_CAT_NAME = "cat_nm";

	// Sub-categories
	public static final String TABLE_SUBCATEGORY = "sub_category";

	public static final String KEY_SUBCAT_ID = "subcat_id";
	public static final String KEY_PARENT_ID = "parent_id";
	public static final String KEY_SUBCAT_NAME = "subcat_nm";

	// Contacts Table Columns names
	private static final String KEY_ID = "id";
	private static final String KEY_NAME = "name";
	private static final String KEY_PH_NO = "phone_number";

	private static final String KEY_BOOKIMG_BLOB = "b_img_blob";
	
	public DatabaseHandler(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}

	// Creating Tables
	@Override
	public void onCreate(SQLiteDatabase db) {
		/*String CREATE_CONTACTS_TABLE = "CREATE TABLE " + TABLE_CONTACTS + "("
				+ KEY_ID + " INTEGER PRIMARY KEY," + KEY_NAME + " TEXT,"
				+ KEY_PH_NO + " TEXT" + ")";
		db.execSQL(CREATE_CONTACTS_TABLE);*/
		
		String CREATE_CATEGORIES_TABLE = "CREATE TABLE " + TABLE_CATEGORY + "("
				+ KEY_CAT_ID + " INTEGER PRIMARY KEY,"
				+ KEY_CAT_NAME + " TEXT" + ")";
		
		
		String CREATE_SUBCATEGORIES_TABLE = "CREATE TABLE " + TABLE_SUBCATEGORY + "("
				+ KEY_SUBCAT_ID + " INTEGER PRIMARY KEY," + KEY_PARENT_ID + " INTEGER,"
				+ KEY_SUBCAT_NAME + " TEXT" + ")";
		
		
		String CREATE_BOOKS_TABLE = "CREATE TABLE " + TABLE_BOOKS + "("
				+ KEY_BOOKID + " INTEGER PRIMARY KEY," + KEY_BOOKNAME + " TEXT," + KEY_BOOKSUBCAT + " INTEGER,"
				+ KEY_BOOKDESC + " TEXT," + KEY_BOOKPUBLISHER + " TEXT," + KEY_BOOKEDITION + " TEXT,"
				+ KEY_BOOKISBN + " TEXT," + KEY_BOOKPAGE + " INTEGER," + KEY_BOOKPRICE + " INTEGER," + KEY_BOOKIMG + " TEXT,"
				+ KEY_BOOKPDF + " TEXT," + KEY_BOOK_SUBCATID + " INTEGER," + KEY_BOOK_PARENTID + " INTEGER,"
				+ KEY_BOOK_SUBCATNAME + " TEXT," + KEY_BOOKIMG_BLOB + " BLOB" + ")";
		
		db.execSQL(CREATE_CATEGORIES_TABLE);
		db.execSQL(CREATE_SUBCATEGORIES_TABLE);
		db.execSQL(CREATE_BOOKS_TABLE);
		
	}

	// Upgrading database
	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// Drop older table if existed
		//db.execSQL("DROP TABLE IF EXISTS " + TABLE_CONTACTS);
		
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_CATEGORY);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_SUBCATEGORY);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_BOOKS);
		
		// Create tables again
		onCreate(db);
	}

	/*
	 * All CRUD(Create, Read, Update, Delete) Operations
	 */
	
	void addDataCategory(ArrayList<Category> catList){
		SQLiteDatabase db = this.getWritableDatabase();
		
		Category cat = new Category();
		ContentValues values = new ContentValues();
		
		for(int i=0;i<catList.size();i++){
			
			cat = catList.get(i);
			
			values.put(KEY_CAT_ID, cat.getCat_id());
			values.put(KEY_CAT_NAME, cat.getCat_name());
		}
		
		
		db.insert(TABLE_CATEGORY, null, values);
	    db.close();
		
	}
	
	void addDataCategory1(Category cat){
		SQLiteDatabase db = this.getWritableDatabase();
		
		ContentValues values = new ContentValues();
		
		values.put(KEY_CAT_ID, cat.getCat_id());
		values.put(KEY_CAT_NAME, cat.getCat_name());
		
		db.insert(TABLE_CATEGORY, null, values);
	    db.close();
		
	}
	
	/*void addDataCategory1(ArrayList<Category> catList){
		SQLiteDatabase db = this.getWritableDatabase();
		Log.e("Entered Data adding", "Entered Data adding");
		ContentValues values = new ContentValues();
		for(int i=0;i<catList.size();i++)
		{
			Log.e("Data added", "data added of"+i);
		values.put(KEY_CAT_ID, catList.get(i).getCat_id());
		values.put(KEY_CAT_NAME, catList.get(i).getCat_name());
		db.insert(TABLE_CATEGORY, null, values);
		}
		Log.e("Exiting for loop inside db", "Exiting for loop inside db");
		//db.insert(TABLE_CATEGORY, null, values);
		Log.e("Database inserted", "Database inserted");
		
	    db.close();
		
	}*/
	
	void addDataSubcategory1(Subcategory subcat){
		SQLiteDatabase db = this.getWritableDatabase();
		
		ContentValues values = new ContentValues();
		values.put(KEY_SUBCAT_ID, subcat.getSubcat_id());
		values.put(KEY_PARENT_ID, subcat.getParent_id());
		values.put(KEY_SUBCAT_NAME, subcat.getSubcat_name());
		
		db.insert(TABLE_SUBCATEGORY, null, values);
	    db.close();
	}
	
	void addDataSubcategory(ArrayList<Subcategory> subcat){
		SQLiteDatabase db = this.getWritableDatabase();
		
		ContentValues values = new ContentValues();
		for(int i=0;i<subcat.size();i++)
		{
		values.put(KEY_SUBCAT_ID, subcat.get(i).getSubcat_id());
		values.put(KEY_PARENT_ID, subcat.get(i).getParent_id());
		values.put(KEY_SUBCAT_NAME, subcat.get(i).getSubcat_name());
		db.insert(TABLE_SUBCATEGORY, null, values);
		}
		//db.insert(TABLE_SUBCATEGORY, null, values);
	    db.close();
	}
	
	void addDataBooks(Books books){
		SQLiteDatabase db = this.getWritableDatabase();
		
		ContentValues values = new ContentValues();
		values.put(KEY_BOOKID, books.getBook_id());
		values.put(KEY_BOOKNAME, books.getBook_name());
		values.put(KEY_BOOKSUBCAT, books.getBook_subcat());
		values.put(KEY_BOOKDESC, books.getBook_desc());
		values.put(KEY_BOOKPUBLISHER, books.getBook_publisher());
		values.put(KEY_BOOKEDITION, books.getBook_edition());
		values.put(KEY_BOOKISBN, books.getBook_isbn());
		values.put(KEY_BOOKPAGE, books.getBook_pages());
		values.put(KEY_BOOKPRICE, books.getBook_price());
		values.put(KEY_BOOKIMG, books.getBook_image());
		values.put(KEY_BOOKPDF, books.getBook_pdf());
		values.put(KEY_BOOK_SUBCATID, books.getBook_subcat_id());
		values.put(KEY_BOOK_PARENTID, books.getBook_parent_id());
		values.put(KEY_BOOK_SUBCATNAME, books.getBook_subcat_name());
		values.put(KEY_BOOKIMG_BLOB, books.getBimg());
		
		db.insert(TABLE_BOOKS, null, values);
	    db.close();
	}
	
	
	

	// Adding new contact
	/*void addContact(Contact contact) {
		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(KEY_NAME, contact.getName()); // Contact Name
		values.put(KEY_PH_NO, contact.getPhoneNumber()); // Contact Phone

		// Inserting Row
		db.insert(TABLE_CONTACTS, null, values);
		db.close(); // Closing database connection
	}*/

	// Getting single contact
	/*Contact getContact(int id) {
		SQLiteDatabase db = this.getReadableDatabase();

		Cursor cursor = db.query(TABLE_CONTACTS, new String[] { KEY_ID,
				KEY_NAME, KEY_PH_NO }, KEY_ID + "=?",
				new String[] { String.valueOf(id) }, null, null, null, null);
		if (cursor != null)
			cursor.moveToFirst();

		Contact contact = new Contact(Integer.parseInt(cursor.getString(0)),
				cursor.getString(1), cursor.getString(2));
		// return contact
		return contact;
	}*/
	
	// Getting All Contacts
	/*public List<Contact> getAllContacts() {
		List<Contact> contactList = new ArrayList<Contact>();
		// Select All Query
		String selectQuery = "SELECT  * FROM " + TABLE_CONTACTS;

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				Contact contact = new Contact();
				contact.setID(Integer.parseInt(cursor.getString(0)));
				contact.setName(cursor.getString(1));
				contact.setPhoneNumber(cursor.getString(2));
				// Adding contact to list
				contactList.add(contact);
			} while (cursor.moveToNext());
		}

		// return contact list
		return contactList;
	}*/

	// Updating single contact
	/*public int updateContact(Contact contact) {
		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(KEY_NAME, contact.getName());
		values.put(KEY_PH_NO, contact.getPhoneNumber());

		// updating row
		return db.update(TABLE_CONTACTS, values, KEY_ID + " = ?",
				new String[] { String.valueOf(contact.getID()) });
	}*/

	// Deleting single contact
	/*public void deleteContact(Contact contact) {
		SQLiteDatabase db = this.getWritableDatabase();
		db.delete(TABLE_CONTACTS, KEY_ID + " = ?",
				new String[] { String.valueOf(contact.getID()) });
		db.close();
	}*/


	// Getting contacts Count
	/*public int getContactsCount() {
		String countQuery = "SELECT  * FROM " + TABLE_CONTACTS;
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor cursor = db.rawQuery(countQuery, null);
		cursor.close();

		// return count
		return cursor.getCount();
	}*/

	public ArrayList<Category> getCategoriesName() {
		ArrayList<Category> categoryList = new ArrayList<Category>();
		// Select All Query
		String selectQuery = "SELECT  * FROM " + TABLE_CATEGORY;
		Log.e("Entered Data read", "Entered Data read");
		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);
		

		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				Category cat = new Category();
				cat.setCat_id(Integer.parseInt(cursor.getString(0)));
				cat.setCat_name(cursor.getString(1));
				// Adding categories to list
				categoryList.add(cat);
			} while (cursor.moveToNext());
		}
		Log.e("Data read finished", "Data read finished");
		
		db.close();
		// return category list
		return categoryList;
	}
	
	public ArrayList<Subcategory> getSubcategoriesName() {
		ArrayList<Subcategory> subcategoryList = new ArrayList<Subcategory>();
		// Select All Query
		String selectQuery = "SELECT  * FROM " + TABLE_SUBCATEGORY;

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				Subcategory subcat = new Subcategory();
				subcat.setSubcat_id(Integer.parseInt(cursor.getString(0)));
				subcat.setParent_id(Integer.parseInt(cursor.getString(1)));
				subcat.setSubcat_name(cursor.getString(2));
				// Adding categories to list
				subcategoryList.add(subcat);
			} while (cursor.moveToNext());
		}
		db.close();
		// return category list
		return subcategoryList;
	}
	
	public ArrayList<Subcategory> getSubcategoriesSelectedName(int catId) {
		ArrayList<Subcategory> subcategoryList = new ArrayList<Subcategory>();
		// Select All Query
		String selectQuery = "SELECT  * FROM " + TABLE_SUBCATEGORY + " WHERE " + KEY_PARENT_ID + " = " + catId;

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				Subcategory subcat = new Subcategory();
				subcat.setSubcat_id(Integer.parseInt(cursor.getString(0)));
				subcat.setParent_id(Integer.parseInt(cursor.getString(1)));
				subcat.setSubcat_name(cursor.getString(2));
				// Adding categories to list
				subcategoryList.add(subcat);
			} while (cursor.moveToNext());
		}
		db.close();
		// return category list
		return subcategoryList;
	}
	
	public ArrayList<Books> getBooksDetails() {
		ArrayList<Books> bookList = new ArrayList<Books>();
		// Select All Query
		String selectQuery = "SELECT  * FROM " + TABLE_BOOKS;

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);
		
		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				Books bookdetails = new Books();
				bookdetails.setBook_id(Integer.parseInt(cursor.getString(0)));
				bookdetails.setBook_name(cursor.getString(1));
				bookdetails.setBook_subcat(Integer.parseInt(cursor.getString(2)));
				bookdetails.setBook_desc(cursor.getString(3));
				bookdetails.setBook_publisher(cursor.getString(4));
				bookdetails.setBook_edition(cursor.getString(5));
				bookdetails.setBook_isbn(cursor.getString(6));
				bookdetails.setBook_pages(Integer.parseInt(cursor.getString(7)));
				bookdetails.setBook_price(Integer.parseInt(cursor.getString(8)));
				bookdetails.setBook_image(cursor.getString(9));
				bookdetails.setBook_pdf(cursor.getString(10));
				bookdetails.setBook_subcat_id(Integer.parseInt(cursor.getString(11)));
				bookdetails.setBook_parent_id(Integer.parseInt(cursor.getString(12)));
				bookdetails.setBook_subcat_name(cursor.getString(13));
				bookdetails.setBimg(cursor.getBlob(14));
				// Adding categories to list
				bookList.add(bookdetails);
			} while (cursor.moveToNext());
		}
		db.close();
		// return category list
		return bookList;
	}
	
	public ArrayList<Books> getBooksBySubCategory(int subcatId) {
		ArrayList<Books> booksByCatList = new ArrayList<Books>();
		// Select All Query
		String selectQuery = "SELECT  * FROM " + TABLE_BOOKS + " WHERE " + KEY_BOOKSUBCAT + " = " + subcatId;

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);
		//Books bookdetails = new Books();
		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				Books bookdetails = new Books();
				bookdetails.setBook_id(Integer.parseInt(cursor.getString(0)));
				bookdetails.setBook_name(cursor.getString(1));
				bookdetails.setBook_subcat(Integer.parseInt(cursor.getString(2)));
				bookdetails.setBook_desc(cursor.getString(3));
				bookdetails.setBook_publisher(cursor.getString(4));
				bookdetails.setBook_edition(cursor.getString(5));
				bookdetails.setBook_isbn(cursor.getString(6));
				bookdetails.setBook_pages(Integer.parseInt(cursor.getString(7)));
				bookdetails.setBook_price(Integer.parseInt(cursor.getString(8)));
				bookdetails.setBook_image(cursor.getString(9));
				bookdetails.setBook_pdf(cursor.getString(10));
				bookdetails.setBook_subcat_id(Integer.parseInt(cursor.getString(11)));
				bookdetails.setBook_parent_id(Integer.parseInt(cursor.getString(12)));
				bookdetails.setBook_subcat_name(cursor.getString(13));
				bookdetails.setBimg(cursor.getBlob(14));
				// Adding categories to list
				booksByCatList.add(bookdetails);
			} while (cursor.moveToNext());
		}
		db.close();
		// return category list
		return booksByCatList;
	}
	
	/*public ArrayList<Books> getBookames(int subcatId) {
		ArrayList<Books> booksByCatList = new ArrayList<Books>();
		// Select All Query
		String selectQuery = "SELECT * FROM " + TABLE_BOOKS + " WHERE " + KEY_BOOK_SUBCATID + " = " + subcatId;

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);
		
		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				Books bookdetails = new Books();
				bookdetails.setBook_id(Integer.parseInt(cursor.getString(0)));
				bookdetails.setBook_name(cursor.getString(1));
				bookdetails.setBook_subcat(Integer.parseInt(cursor.getString(2)));
				bookdetails.setBook_desc(cursor.getString(3));
				bookdetails.setBook_publisher(cursor.getString(4));
				bookdetails.setBook_edition(cursor.getString(5));
				bookdetails.setBook_isbn(cursor.getString(6));
				bookdetails.setBook_pages(Integer.parseInt(cursor.getString(7)));
				bookdetails.setBook_price(Integer.parseInt(cursor.getString(8)));
				bookdetails.setBook_image(cursor.getString(9));
				bookdetails.setBook_pdf(cursor.getString(10));
				bookdetails.setBook_subcat_id(Integer.parseInt(cursor.getString(11)));
				bookdetails.setBook_parent_id(Integer.parseInt(cursor.getString(12)));
				bookdetails.setBook_subcat_name(cursor.getString(13));
				// Adding categories to list
				booksByCatList.add(bookdetails);
			} while (cursor.moveToNext());
		}
		db.close();
		// return category list
		return booksByCatList;
	}*/
	
	public Object getSingleCategory(int catId) {
		
		// Select All Query
		String selectQuery = "SELECT * FROM " + TABLE_CATEGORY + " WHERE " + KEY_CAT_ID + " = " + catId;

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);
		Category cat = new Category();
		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				
				cat.setCat_id(Integer.getInteger(cursor.getString(0)));
				cat.setCat_name(cursor.getString(1));
				// Adding categories to list
			} while (cursor.moveToNext());
		}
		db.close();
		// return category list
		return cat;
	}
	
	
	
	public ArrayList<Subcategory> getSubcategoryList(int parentId) {
		
		// Select All Query
		String selectQuery = "SELECT * FROM " + TABLE_SUBCATEGORY + " WHERE " + KEY_PARENT_ID + " = " + parentId;

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);
		
		ArrayList<Subcategory> listSubcat = new ArrayList<Subcategory>();
		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				Subcategory subcat = new Subcategory();
				subcat.setSubcat_id(Integer.parseInt(cursor.getString(0)));
				subcat.setParent_id(Integer.parseInt(cursor.getString(1)));
				subcat.setSubcat_name(cursor.getString(2));
				
				listSubcat.add(subcat);
				// Adding categories to list
			} while (cursor.moveToNext());
		}
		db.close();
		// return category list
		return listSubcat;
	}

	public Object getSingleBook(int bookId) {
	
	// Select All Query
	String selectQuery = "SELECT * FROM " + TABLE_SUBCATEGORY + " WHERE " + KEY_BOOKID + " = " + bookId;

	SQLiteDatabase db = this.getWritableDatabase();
	Cursor cursor = db.rawQuery(selectQuery, null);
	Books book = new Books();
	// looping through all rows and adding to list
	if (cursor.moveToFirst()) {
		do {
			
			book.setBook_id(Integer.parseInt(cursor.getString(0)));
			book.setBook_name(cursor.getString(1));
			book.setBook_subcat(Integer.parseInt(cursor.getString(2)));
			book.setBook_desc(cursor.getString(3));
			book.setBook_publisher(cursor.getString(4));
			book.setBook_edition(cursor.getString(5));
			book.setBook_isbn(cursor.getString(6));
			book.setBook_pages(Integer.parseInt(cursor.getString(7)));
			book.setBook_price(Integer.parseInt(cursor.getString(8)));
			book.setBook_image(cursor.getString(9));
			book.setBook_pdf(cursor.getString(10));
			book.setBook_subcat_id(Integer.parseInt(cursor.getString(11)));
			book.setBook_parent_id(Integer.parseInt(cursor.getString(12)));
			book.setBook_subcat_name(cursor.getString(13));
			book.setBimg(cursor.getBlob(14));
			// Adding categories to list
		} while (cursor.moveToNext());
	}
	db.close();
	// return category list
	return book;
	}
	
	/*public void addEntry( String name, byte[] image) throws SQLiteException{
	    ContentValues cv = new  ContentValues();
	    cv.put(KEY_NAME,    name);
	    cv.put(KEY_IMAGE,   image);
	    database.insert( DB_TABLE, null, cv );
	}

	
	
	// convert from bitmap to byte array
    public static byte[] getBytes(Bitmap bitmap) {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(CompressFormat.PNG, 0, stream);
        return stream.toByteArray();
    }

    // convert from byte array to bitmap
    public static Bitmap getImage(byte[] image) {
        return BitmapFactory.decodeByteArray(image, 0, image.length);
    }*/
	
	
}
