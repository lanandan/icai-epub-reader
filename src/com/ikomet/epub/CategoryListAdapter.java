package com.ikomet.epub;

import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.ikomet.epub.pojo.Category;
import com.skytree.epubtest.R;

public class CategoryListAdapter extends BaseAdapter{

	ArrayList<Category> data;
	Context ctx;
	LayoutInflater inflater = null;
	
	public CategoryListAdapter(Context ctx, ArrayList<Category> data){
		this.data = data;
		this.ctx = ctx;
	}
	
	
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return data.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return data.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}
	
	class ViewHolder{
		TextView txt;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		
		ViewHolder holder;
		
		if(convertView==null){
			convertView = LayoutInflater.from(ctx).inflate(
					R.layout.custom_list_rowitem, parent, false);
		}
			holder = new ViewHolder();
			holder.txt = (TextView)convertView.findViewById(R.id.txt_customlist);
		
		
		
		holder.txt.setTextColor(ctx.getResources().getColor(R.color.colorPrimary));
		holder.txt.setTextSize(22);
		holder.txt.setText(data.get(position).getCat_name());
		
		return convertView;
	}

}
