package com.ikomet.epub.pojo;

import java.sql.Blob;

public class Books {

	int book_id,book_subcat,book_pages,book_price,book_subcat_id,book_parent_id;
	String book_name,book_desc,book_publisher,book_edition,book_isbn,book_image,book_pdf,book_subcat_name;
	byte[] bimg;	
	
	public Books(){
		
	}
	
	public Books(int book_id, int book_subcat, int book_pages, int book_price,
			String book_name, String book_desc, String book_publisher,
			String book_edition, String book_isbn, String book_image,
			String book_pdf, byte[] bimg) {
		super();
		this.book_id = book_id;
		this.book_subcat = book_subcat;
		this.book_pages = book_pages;
		this.book_price = book_price;
		this.book_name = book_name;
		this.book_desc = book_desc;
		this.book_publisher = book_publisher;
		this.book_edition = book_edition;
		this.book_isbn = book_isbn;
		this.book_image = book_image;
		this.book_pdf = book_pdf;
		this.bimg = bimg;
	}
	public int getBook_id() {
		return book_id;
	}
	public void setBook_id(int book_id) {
		this.book_id = book_id;
	}
	public int getBook_subcat() {
		return book_subcat;
	}
	public void setBook_subcat(int book_subcat) {
		this.book_subcat = book_subcat;
	}
	public int getBook_pages() {
		return book_pages;
	}
	public void setBook_pages(int book_pages) {
		this.book_pages = book_pages;
	}
	public int getBook_price() {
		return book_price;
	}
	public void setBook_price(int book_price) {
		this.book_price = book_price;
	}
	public int getBook_subcat_id() {
		return book_subcat_id;
	}
	public void setBook_subcat_id(int book_subcat_id) {
		this.book_subcat_id = book_subcat_id;
	}
	public int getBook_parent_id() {
		return book_parent_id;
	}
	public void setBook_parent_id(int book_parent_id) {
		this.book_parent_id = book_parent_id;
	}
	public String getBook_name() {
		return book_name;
	}
	public void setBook_name(String book_name) {
		this.book_name = book_name;
	}
	public String getBook_desc() {
		return book_desc;
	}
	public void setBook_desc(String book_desc) {
		this.book_desc = book_desc;
	}
	public String getBook_publisher() {
		return book_publisher;
	}
	public void setBook_publisher(String book_publisher) {
		this.book_publisher = book_publisher;
	}
	public String getBook_edition() {
		return book_edition;
	}
	public void setBook_edition(String book_edition) {
		this.book_edition = book_edition;
	}
	public String getBook_isbn() {
		return book_isbn;
	}
	public void setBook_isbn(String book_isbn) {
		this.book_isbn = book_isbn;
	}
	public String getBook_image() {
		return book_image;
	}
	public void setBook_image(String book_image) {
		this.book_image = book_image;
	}
	public String getBook_pdf() {
		return book_pdf;
	}
	public void setBook_pdf(String book_pdf) {
		this.book_pdf = book_pdf;
	}
	public String getBook_subcat_name() {
		return book_subcat_name;
	}
	public void setBook_subcat_name(String book_subcat_name) {
		this.book_subcat_name = book_subcat_name;
	}

	public byte[] getBimg() {
		return bimg;
	}

	public void setBimg(byte[] bimg) {
		this.bimg = bimg;
	}
	
	
}
