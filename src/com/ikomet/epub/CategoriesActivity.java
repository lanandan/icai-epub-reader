package com.ikomet.epub;

import java.util.ArrayList;
import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.ikomet.epub.pojo.Category;
import com.ikomet.epub.pojo.Subcategory;
import com.ikomet.epub.utils.NoInternet;
import com.skytree.epubtest.R;

public class CategoriesActivity extends Activity {

	public static int cat_sel_pos, subcat_sel_pos;

	TextView titleCat, logout, txtwelcome;
	ListView listofCat;
	CategoryListAdapter category_adapter = null;

	// private ProgressDialog pDialog;
	TransparentProgressDialog tpd;

	// URL to get contacts JSON
	// public static String url =
	// "http://storybook.verifiedwork.com/webservices/get_book_details.php";
	public static String url = "http://storybook.verifiedwork.com/webservices/category_details.php";
	// JSON Node names
	// Books
	public static final String TAG_BOOKS = "books";

	public static final String TAG_BOOKID = "b_id";
	public static final String TAG_BOOKNAME = "b_nm";
	public static final String TAG_BOOKSUBCAT = "b_subcat";
	public static final String TAG_BOOKDESC = "b_desc";
	public static final String TAG_BOOKPUBLISHER = "b_publisher";
	public static final String TAG_BOOKEDITION = "b_edition";
	public static final String TAG_BOOKISBN = "b_isbn";
	public static final String TAG_BOOKPAGE = "b_page";
	public static final String TAG_BOOKPRICE = "b_price";
	public static final String TAG_BOOKIMG = "b_img";
	public static final String TAG_BOOKPDF = "b_pdf";
	public static final String TAG_BOOK_SUBCATID = "subcat_id";
	public static final String TAG_BOOK_PARENTID = "parent_id";
	public static final String TAG_BOOK_SUBCATNAME = "subcat_nm";

	// Categories
	public static final String TAG_CATEGORY = "category";

	public static final String TAG_CAT_ID = "cat_id";
	public static final String TAG_CAT_NAME = "cat_nm";

	// Sub-categories
	public static final String TAG_SUBCATEGORY = "sub_category";

	public static final String TAG_SUBCAT_ID = "subcat_id";
	public static final String TAG_PARENT_ID = "parent_id";
	public static final String TAG_SUBCAT_NAME = "subcat_nm";

	// contacts JSONArray
	JSONArray categories = null, subcategories = null, imagesurlList = null;

	public static ArrayList<String> cat_names, subcat_names;
	ArrayList<Integer> cat_ids;
	ArrayList<HashMap<String, String>> imagesString;
	public static String imgbs64;
	public static String cacheJSONString = null;
	SharedPreferences prefLogin;
	public static final String MyPREFERENCES = "MyPrefs";
	public static ArrayList<Bitmap> bookCoverslist;
	public static String jsonDataString;

	Category cat;
	ArrayList<Category> lst_cat_update, lst_cat_getfrom;
	public static ArrayList<Category> categoriesList;
	public static ArrayList<Subcategory> subcategoriesList;

	// public static int sh_i=0;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_categories);

		/*
		 * Intent intent = new Intent(CategoriesActivity.this,
		 * SplashActivity.class); startActivity(intent);
		 */
		// sharedpreferences = getSharedPreferences(MyPREFERENCES,
		// Context.MODE_WORLD_READABLE);
		/*
		 * prefLogin = getSharedPreferences(MyPREFERENCES,
		 * Context.MODE_PRIVATE); String uname =
		 * prefLogin.getString(LoginActivity.UNAME, "");
		 */

		txtwelcome = (TextView) findViewById(R.id.txt_welcome);

		txtwelcome.setText("Welcome " + LoginActivity.storedUsrnm);

		listofCat = (ListView) findViewById(R.id.lst_categories);

		logout = (TextView) findViewById(R.id.txt_logout);

		lst_cat_update = new ArrayList<Category>();
		lst_cat_getfrom = new ArrayList<Category>();
		cat_names = new ArrayList<String>();
		subcat_names = new ArrayList<String>();
		cat_ids = new ArrayList<Integer>();

		categoriesList = new ArrayList<Category>();
		subcategoriesList = new ArrayList<Subcategory>();

		/*
		 * SharedPreferences.Editor editor = shprf.edit();
		 * 
		 * editor.putInt("RunCheck", true); editor.commit();
		 */

		/*
		 * if(shprf1.getBoolean("RunCheck", true)){
		 * //Toast.makeText(getApplicationContext(), "first part", 1000).show();
		 * if(!isInternetConnected (this)){
		 * 
		 * new AlertDialog.Builder(this) .setTitle("Network Check")
		 * .setMessage("Internet Connection is not Enabled in your device."
		 * ).setCancelable(false) .setPositiveButton("Enable MobileData", new
		 * DialogInterface.OnClickListener(){ public void
		 * onClick(DialogInterface dialog, int which) { // goto settings to
		 * enable it Intent intent = new
		 * Intent(android.provider.Settings.ACTION_WIRELESS_SETTINGS);
		 * intent.setFlags(Intent.FLAG_ACTIVITY_LAUNCHED_FROM_HISTORY);
		 * startActivity(intent); } }) .setNegativeButton("Enable wifi", new
		 * DialogInterface.OnClickListener() { public void
		 * onClick(DialogInterface dialog, int which) { // exit application
		 * Intent intent = new
		 * Intent(android.provider.Settings.ACTION_WIFI_SETTINGS);
		 * intent.setFlags(Intent.FLAG_ACTIVITY_LAUNCHED_FROM_HISTORY);
		 * startActivity(intent); } })
		 * .setIcon(android.R.drawable.ic_dialog_alert) .show();
		 * 
		 * 
		 * 
		 * }else{
		 * 
		 * new GetBooksData().execute();
		 * 
		 * SharedPreferences.Editor editor = shprf1.edit();
		 * 
		 * editor.putBoolean("RunCheck", false); editor.commit();
		 * 
		 * }
		 * 
		 * 
		 * 
		 * 
		 * 
		 * }else { //Toast.makeText(getApplicationContext(), "second part",
		 * 1000).show(); new GetBooksData().execute(); }
		 */
		new GetBooksData().execute();
		listofCat.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				// TODO Auto-generated method stub

				// int category_id = lst_cat_getfrom.get(position).getCat_id();

				int category_id = categoriesList.get(position).getCat_id();
				cat_sel_pos = position;
				Intent intent = new Intent(CategoriesActivity.this,
						SubCategoriesActivity.class);
				intent.putExtra("category_id", category_id);
				intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
				startActivity(intent);
				overridePendingTransition(R.anim.right_in, R.anim.left_out);
			}
		});

		logout.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				finish();
				Intent intent = new Intent(CategoriesActivity.this,
						LoginActivity.class);
				LoginActivity.prefLogin.edit().clear().commit();
				intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK
						| Intent.FLAG_ACTIVITY_CLEAR_TASK);
				startActivity(intent);
			}
		});

	}

	private class GetBooksData extends AsyncTask<Void, Void, Void> {

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			// Showing progress dialog
			tpd = new TransparentProgressDialog(CategoriesActivity.this,
					R.drawable.icai1);
			tpd.show();

		}

		@Override
		protected Void doInBackground(Void... arg0) {

			/*
			 * ServiceHandler sh = new ServiceHandler();
			 * 
			 * String jsonStr = sh.makeServiceCall(url, ServiceHandler.GET);
			 */

			ServiceHandler sh;

			String jsonStr;
			Log.e("dib", "inside do in backgnd");

			DatabaseHandler dbh = new DatabaseHandler(CategoriesActivity.this);
			categoriesList = new ArrayList<Category>();

			if (dbh.getCategoriesName().size() > 0) {
				categoriesList = dbh.getCategoriesName();

			} else {
				if (isInternetConnected(getApplicationContext())) {

					sh = new ServiceHandler();

					jsonStr = sh.makeServiceCall(url, ServiceHandler.GET);

					if (!jsonStr.equals(null)) {

						try {
							JSONObject jsonObj = new JSONObject(jsonStr);

							Log.d("Json String", "" + jsonStr);

							categories = jsonObj.getJSONArray(TAG_CATEGORY);

							subcategories = jsonObj
									.getJSONArray(TAG_SUBCATEGORY);

							// DatabaseHandler dbh1 = new
							// DatabaseHandler(CategoriesActivity.this);
							// categoriesList = new ArrayList<Category>();

							for (int i = 0; i < categories.length(); i++) {
								Log.e("for", "inside for loop");
								JSONObject c = categories.getJSONObject(i);
								Log.d("OKAY", "" + categories.getJSONObject(i));
								int cat_id = Integer.parseInt(c
										.getString(TAG_CAT_ID));
								String cat_name = c.getString(TAG_CAT_NAME);
								Log.d("Checking", "" + cat_id + "   "
										+ cat_name);
								// cat_names.add(cat_name);
								// lst_cat_update.add(new
								// Category(cat_id,cat_name));
								// dbh1.addDataCategory1(new
								// Category(cat_id,cat_name));
								DatabaseHandler dbh1 = new DatabaseHandler(
										CategoriesActivity.this);
								dbh1.addDataCategory1(new Category(cat_id,
										cat_name));
								categoriesList.add(new Category(cat_id,
										cat_name));
							}

							// dbh1.addDataCategory1(categoriesList);
							// DatabaseHandler dbh2 = new
							// DatabaseHandler(CategoriesActivity.this);
							for (int j = 0; j < subcategories.length(); j++) {
								JSONObject sc = subcategories.getJSONObject(j);
								int subcat_id = Integer.parseInt(sc
										.getString(TAG_SUBCAT_ID));
								int parent_id = Integer.parseInt(sc
										.getString(TAG_PARENT_ID));
								String subcat_name = sc
										.getString(TAG_SUBCAT_NAME);
								// subcat_names.add(subcat_name);
								DatabaseHandler dbh1 = new DatabaseHandler(
										CategoriesActivity.this);
								dbh1.addDataSubcategory1(new Subcategory(
										subcat_id, parent_id, subcat_name));
								subcategoriesList.add(new Subcategory(
										subcat_id, parent_id, subcat_name));

							}

							// dbh2.addDataSubcategory(subcategoriesList);

							Log.e("before", "before adding in database");
							// dbh1.addDataCategory1(categoriesList);
							Log.e("after", "after adding in database");
							// DatabaseHandler dbh2 = new
							// DatabaseHandler(CategoriesActivity.this);

							// lst_cat_getfrom = dbh1.getCategoriesName();

							/*
							 * for(int i =0;i<lst_cat_getfrom.size();i++){
							 * cat_names
							 * .add(i,lst_cat_getfrom.get(i).getCat_name());
							 * cat_ids
							 * .add(i,lst_cat_getfrom.get(i).getCat_id()); }
							 */

						} catch (JSONException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}

					}

				} else {

					NoInternet
							.internetConnectionFinder(CategoriesActivity.this);

				}

			}

			/*
			 * if(categoriesList.size()>0){
			 * 
			 * for(int i=0;i<categoriesList.size();i++){
			 * //cat_names.add(categoriesList.get(i).getCat_name());
			 * 
			 * }
			 * 
			 * 
			 * }else{ }
			 */

			/*
			 * DatabaseHandler dbh = new
			 * DatabaseHandler(CategoriesActivity.this);
			 * 
			 * ArrayList<Category> catList = dbh.getCategoriesName();
			 * Log.e("catlist", "catlistread"); if(catList.isEmpty()){
			 * Log.e("empty", "catlist is empty");
			 */
			/*
			 * if(isInternetConnected(getApplicationContext())){
			 * 
			 * sh = new ServiceHandler();
			 * 
			 * jsonStr = sh.makeServiceCall(url, ServiceHandler.GET);
			 * 
			 * if(!jsonStr.equals(null)){
			 * 
			 * try { JSONObject jsonObj = new JSONObject(jsonStr);
			 * 
			 * Log.d("Json String",""+jsonStr);
			 * 
			 * categories = jsonObj.getJSONArray(TAG_CATEGORY);
			 * 
			 * subcategories = jsonObj.getJSONArray(TAG_SUBCATEGORY);
			 * 
			 * //DatabaseHandler dbh1 = new
			 * DatabaseHandler(CategoriesActivity.this); //categoriesList = new
			 * ArrayList<Category>();
			 * 
			 * for (int i = 0; i < categories.length(); i++) { Log.e("for",
			 * "inside for loop"); JSONObject c = categories.getJSONObject(i);
			 * Log.d("OKAY", ""+categories.getJSONObject(i)); int cat_id =
			 * Integer.parseInt(c.getString(TAG_CAT_ID)); String cat_name =
			 * c.getString(TAG_CAT_NAME); Log.d("Checking",
			 * ""+cat_id+"   "+cat_name); cat_names.add(cat_name);
			 * //lst_cat_update.add(new Category(cat_id,cat_name));
			 * //dbh1.addDataCategory1(new Category(cat_id,cat_name));
			 * 
			 * categoriesList.add(new Category(cat_id,cat_name)); }
			 * 
			 * //dbh1.addDataCategory1(categoriesList); //DatabaseHandler dbh2 =
			 * new DatabaseHandler(CategoriesActivity.this); for(int
			 * j=0;j<subcategories.length();j++){ JSONObject sc =
			 * subcategories.getJSONObject(j); int subcat_id =
			 * Integer.parseInt(sc.getString(TAG_SUBCAT_ID)); int parent_id =
			 * Integer.parseInt(sc.getString(TAG_PARENT_ID)); String subcat_name
			 * = sc.getString(TAG_SUBCAT_NAME); subcat_names.add(subcat_name);
			 * subcategoriesList.add(new Subcategory(subcat_id, parent_id,
			 * subcat_name));
			 * 
			 * }
			 * 
			 * //dbh2.addDataSubcategory(subcategoriesList);
			 * 
			 * Log.e("before", "before adding in database");
			 * //dbh1.addDataCategory1(categoriesList); Log.e("after",
			 * "after adding in database"); //DatabaseHandler dbh2 = new
			 * DatabaseHandler(CategoriesActivity.this);
			 * 
			 * //lst_cat_getfrom = dbh1.getCategoriesName();
			 * 
			 * for(int i =0;i<lst_cat_getfrom.size();i++){
			 * cat_names.add(i,lst_cat_getfrom.get(i).getCat_name());
			 * cat_ids.add(i,lst_cat_getfrom.get(i).getCat_id()); }
			 * 
			 * } catch (JSONException e) { // TODO Auto-generated catch block
			 * e.printStackTrace(); }
			 * 
			 * }
			 * 
			 * 
			 * }
			 */
			// }
			/* } */
			/*
			 * else{
			 * 
			 * //DatabaseHandler dbh2 = new
			 * DatabaseHandler(CategoriesActivity.this);
			 * 
			 * lst_cat_getfrom = dbh.getCategoriesName();
			 * 
			 * for(int i =0;i<lst_cat_getfrom.size();i++){
			 * cat_names.add(lst_cat_getfrom.get(i).getCat_name()); }
			 * 
			 * 
			 * }
			 */

			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
			// Dismiss the progress dialog
			if (tpd.isShowing()) {
				tpd.dismiss();
			}
			category_adapter = new CategoryListAdapter(getApplicationContext(),
					categoriesList);

			listofCat.setAdapter(category_adapter);

		}

	}

	public static boolean isInternetConnected(Context ctx) {
		ConnectivityManager connectivityMgr = (ConnectivityManager) ctx
				.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo wifi = connectivityMgr
				.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
		NetworkInfo mobile = connectivityMgr
				.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

		if (wifi != null) {
			if (wifi.isConnected()) {
				return true;
			}
		}
		if (mobile != null) {
			if (mobile.isConnected()) {
				return true;
			}
		}
		return false;
	}

}
