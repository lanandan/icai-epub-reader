package com.ikomet.epub.pojo;

public class Subcategory {
	
	int subcat_id,parent_id;
	String subcat_name;
	
	public Subcategory(int subcat_id, int parent_id, String subcat_name) {
		super();
		this.subcat_id = subcat_id;
		this.parent_id = parent_id;
		this.subcat_name = subcat_name;
	}
	public Subcategory() {
		// TODO Auto-generated constructor stub
	}
	public int getSubcat_id() {
		return subcat_id;
	}
	public void setSubcat_id(int subcat_id) {
		this.subcat_id = subcat_id;
	}
	public int getParent_id() {
		return parent_id;
	}
	public void setParent_id(int parent_id) {
		this.parent_id = parent_id;
	}
	public String getSubcat_name() {
		return subcat_name;
	}
	public void setSubcat_name(String subcat_name) {
		this.subcat_name = subcat_name;
	}

	
	
}
