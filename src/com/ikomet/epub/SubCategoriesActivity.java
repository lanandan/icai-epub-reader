package com.ikomet.epub;

import java.util.ArrayList;

import org.json.JSONArray;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.ikomet.epub.pojo.Subcategory;
import com.skytree.epubtest.R;

public class SubCategoriesActivity extends Activity {

	TextView titlesubcat, logout, txtwelcome;
	ListView listSubCat;

	// ProgressDialog pDialog;
	TransparentProgressDialog tpd;

	JSONArray sub_categories = null;

	int parent_id_intent;
	ArrayList<String> subcat_names;
	ArrayList<Integer> parent_ids, subcat_ids;
	ArrayList<Subcategory> subcatList;
	SubCategoryListAdapter subcategory_adapter;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_subcategories);

		// titlesubcat = (TextView)findViewById(R.id.title_subcategory);
		listSubCat = (ListView) findViewById(R.id.lst_subcategories);

		logout = (TextView) findViewById(R.id.txt_logout);

		txtwelcome = (TextView) findViewById(R.id.txt_welcome);

		txtwelcome.setText("Welcome " + LoginActivity.storedUsrnm);

		subcatList = new ArrayList<Subcategory>();
		subcat_names = new ArrayList<String>();
		parent_ids = new ArrayList<Integer>();
		subcat_ids = new ArrayList<Integer>();

		parent_id_intent = getIntent().getIntExtra("category_id", 0);

		new GetBooksData().execute();

		listSubCat.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				// TODO Auto-generated method stub
				/*
				 * String get_subcat_id = subcat_ids.get(position); String
				 * get_subcat_name = subcat_names.get(position);
				 * 
				 * Intent intent = new Intent(SubCategoriesActivity.this,
				 * BookListActivity.class); intent.putExtra("subcat_id",
				 * get_subcat_id); intent.putExtra("subcat_name",
				 * get_subcat_name); startActivity(intent);
				 */
				CategoriesActivity.subcat_sel_pos = position;
				if (CategoriesActivity.cat_sel_pos == 0
						&& CategoriesActivity.subcat_sel_pos == 0) {
					int get_subcat_id = subcatList.get(position).getSubcat_id();
					Intent intent = new Intent(SubCategoriesActivity.this,
							BookListActivity.class);
					intent.putExtra("subcat_id", get_subcat_id);
					intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
					startActivity(intent);
					overridePendingTransition(R.anim.right_in, R.anim.left_out);
				} else {
					Toast.makeText(getApplicationContext(),
							"Not part of this build", 1000).show();
				}
			}
		});

		logout.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				// finish();
				Intent intent = new Intent(SubCategoriesActivity.this,
						LoginActivity.class);
				LoginActivity.prefLogin.edit().clear().commit();
				intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
				intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK
						| Intent.FLAG_ACTIVITY_CLEAR_TASK);
				startActivity(intent);
				finish();
				overridePendingTransition(R.anim.left_out, R.anim.right_in);
			}
		});

	}

	/**
	 * Async task class to get json by making HTTP call
	 * */
	private class GetBooksData extends AsyncTask<Void, Void, Void> {

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			// Showing progress dialog
			tpd = new TransparentProgressDialog(SubCategoriesActivity.this,
					R.drawable.icai1);
			tpd.show();
		}

		@Override
		protected Void doInBackground(Void... arg0) {
			// Creating service handler class instance
			// ServiceHandler sh = new ServiceHandler();

			// Making a request to url and getting response
			// String jsonStr = sh.makeServiceCall(CategoriesActivity.url,
			// ServiceHandler.GET);

			// Log.e("Response: ", "> " + jsonStr);

			/*
			 * DatabaseHandler dbh = new
			 * DatabaseHandler(SubCategoriesActivity.this);
			 * 
			 * subcatList =
			 * dbh.getSubcategoryList(getIntent().getIntExtra("category_id",
			 * -1));
			 * 
			 * if(subcatList.size()>0){
			 * 
			 * for(int i=0;i<subcatList.size();i++){
			 * 
			 * subcat_ids.add(subcatList.get(i).getSubcat_id());
			 * subcat_names.add(subcatList.get(i).getSubcat_name());
			 * 
			 * }
			 * 
			 * 
			 * }
			 */

			DatabaseHandler dbh = new DatabaseHandler(
					SubCategoriesActivity.this);

			if (dbh.getSubcategoryList(
					getIntent().getIntExtra("category_id", -1)).size() > 0) {

				subcatList = dbh.getSubcategoryList(getIntent().getIntExtra(
						"category_id", -1));

			}

			/*
			 * DatabaseHandler dbh = new
			 * DatabaseHandler(SubCategoriesActivity.this);
			 * 
			 * ArrayList<Subcategory> listSubcat = new ArrayList<Subcategory>();
			 * 
			 * listSubcat = dbh.getSubcategoryList(parent_id_intent);
			 * 
			 * if(listSubcat.isEmpty()){ Log.e("Nothing stored in database",
			 * "No subcats stored yet");
			 * 
			 * if(CategoriesActivity.isInternetConnected
			 * (SubCategoriesActivity.this)){
			 * 
			 * ServiceHandler sh = new ServiceHandler(); String jsonStr =
			 * sh.makeServiceCall(CategoriesActivity.url, ServiceHandler.GET);
			 * 
			 * if(!jsonStr.equals(null)){
			 * 
			 * 
			 * JSONObject jsonObj; try { jsonObj = new JSONObject(jsonStr);
			 * 
			 * 
			 * Log.d("Json String",""+jsonStr);
			 * 
			 * JSONArray subcategories=null;
			 * 
			 * subcategories =
			 * jsonObj.getJSONArray(CategoriesActivity.TAG_SUBCATEGORY);
			 * 
			 * 
			 * //DatabaseHandler dbh1 = new
			 * DatabaseHandler(SubCategoriesActivity.this);
			 * 
			 * for(int i=0;i<subcategories.length();i++){ JSONObject c =
			 * subcategories.getJSONObject(i);
			 * 
			 * int subcat_id =
			 * Integer.parseInt(c.getString(CategoriesActivity.TAG_SUBCAT_ID));
			 * int parent_id =
			 * Integer.parseInt(c.getString(CategoriesActivity.TAG_PARENT_ID));
			 * String subcat_name =
			 * c.getString(CategoriesActivity.TAG_SUBCAT_NAME);
			 * 
			 * if(parent_id==parent_id_intent){
			 * 
			 * dbh.addDataSubcategory(new
			 * Subcategory(subcat_id,parent_id,subcat_name));
			 * 
			 * } }
			 * 
			 * //DatabaseHandler dbh2 = new
			 * DatabaseHandler(SubCategoriesActivity.this);
			 * 
			 * subcatList = dbh.getSubcategoryList(parent_id_intent);
			 * 
			 * for(int i=0;i<subcatList.size();i++){
			 * subcat_names.add(subcatList.get(i).getSubcat_name());
			 * subcat_ids.add(subcatList.get(i).getSubcat_id());
			 * parent_ids.add(subcatList.get(i).getParent_id());
			 * 
			 * }
			 * 
			 * 
			 * } catch (JSONException e) { // TODO Auto-generated catch block
			 * e.printStackTrace(); } } } else{ new
			 * AlertDialog.Builder(SubCategoriesActivity.this)
			 * .setTitle("Network Check")
			 * .setMessage("Internet Connection is not Enabled in your device.")
			 * .setPositiveButton("Enable it", new
			 * DialogInterface.OnClickListener() { public void
			 * onClick(DialogInterface dialog, int which) { // goto settings to
			 * enable it startActivity(new
			 * Intent(android.provider.Settings.ACTION_WIRELESS_SETTINGS)); } })
			 * .setNegativeButton("Exit", new DialogInterface.OnClickListener()
			 * { public void onClick(DialogInterface dialog, int which) { //
			 * exit application SubCategoriesActivity.this.finish(); } })
			 * .setIcon(android.R.drawable.ic_dialog_alert) .show(); }
			 * 
			 * 
			 * }else{
			 * 
			 * 
			 * 
			 * //DatabaseHandler dbh2 = new
			 * DatabaseHandler(SubCategoriesActivity.this);
			 * 
			 * subcatList = dbh.getSubcategoryList(parent_id_intent);
			 * Log.e("Size of subcategory", ""+subcatList.size()); for(int
			 * i=0;i<subcatList.size();i++){ Log.e("Subcat Name",
			 * ""+subcatList.get(i).getSubcat_name());
			 * subcat_names.add(subcatList.get(i).getSubcat_name());
			 * subcat_ids.add(subcatList.get(i).getSubcat_id());
			 * parent_ids.add(subcatList.get(i).getParent_id());
			 * 
			 * } }
			 */

			/*
			 * if (CategoriesActivity.jsonDataString != null) { try { JSONObject
			 * jsonObj = new JSONObject(CategoriesActivity.jsonDataString);
			 * 
			 * // Getting JSON Array node sub_categories =
			 * jsonObj.getJSONArray(CategoriesActivity.TAG_SUBCATEGORY);
			 * 
			 * subcat_ids = new ArrayList<String>(); subcat_names = new
			 * ArrayList<String>(); // looping through All Contacts for (int i =
			 * 0; i < sub_categories.length(); i++) { JSONObject c =
			 * sub_categories.getJSONObject(i);
			 * 
			 * String parent_id = c.getString(CategoriesActivity.TAG_PARENT_ID);
			 * String subcat_name =
			 * c.getString(CategoriesActivity.TAG_SUBCAT_NAME); String subcat_id
			 * = c.getString(CategoriesActivity.TAG_SUBCAT_ID);
			 * 
			 * if(parent_id.equals(parent_id_intent)) {
			 * subcat_names.add(subcat_name); subcat_ids.add(subcat_id); }
			 * 
			 * } } catch (JSONException e) { e.printStackTrace(); } } else {
			 * Log.e("ServiceHandler", "Couldn't get any data from the url"); }
			 */

			/*
			 * for(int i=0;i<CategoriesActivity.subcategoriesList.size();i++){
			 * if(getIntent().getIntExtra("category_id",
			 * -1)==CategoriesActivity.subcategoriesList.get(i).getParent_id()){
			 * subcat_ids
			 * .add(CategoriesActivity.subcategoriesList.get(i).getSubcat_id());
			 * subcat_names
			 * .add(CategoriesActivity.subcategoriesList.get(i).getSubcat_name
			 * ()); } }
			 */

			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
			// Dismiss the progress dialog

			tpd.dismiss();

			subcategory_adapter = new SubCategoryListAdapter(
					getApplicationContext(), subcatList);

			listSubCat.setAdapter(subcategory_adapter);

		}

	}

}
