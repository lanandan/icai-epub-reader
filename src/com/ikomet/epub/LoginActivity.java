package com.ikomet.epub;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import com.ikomet.epub.utils.NoInternet;
import com.skytree.epubtest.R;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Config;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class LoginActivity extends Activity{

	EditText email,password;
	Button login;
	TextView register, forgot_pass;
	public static SharedPreferences prefLogin,prefBookCheck;
	public static final String MyPREFERENCES = "MyPrefs";
	 // Progress Dialog
    private ProgressDialog pDialog;
    public static final String EMAIL = "email";
    public static final String PWD = "password";
    public static final String UNAME = "username";
    // JSON parser class
    
   // GoogleCloudMessaging gcm;
	Context context;
	String regId;
	String gcm1;

	public static final String REG_ID = "regId";
	private static final String APP_VERSION = "appVersion";
	static final String TAG = "Login Activity";
    
	String user_name;
	
	public static String storedUsrnm;
	
   // JSONParser jsonParser = new JSONParser();

    //php login script location:

    //localhost :
    //testing on your device
    //put your local ip instead,  on windows, run CMD > ipconfig
    //or in mac's terminal type ifconfig and look for the ip under en0 or en1
   // private static final String LOGIN_URL = "http://xxx.xxx.x.x:1234/webservice/login.php";

    //testing on Emulator:
    private static final String LOGIN_URL = "http://storybook.verifiedwork.com/webservices/user_login.php";
 

  //testing from a real server:
    //private static final String LOGIN_URL = "http://www.yourdomain.com/webservice/login.php";

    //JSON element ids from repsonse of php script:
    private static final String TAG_SUCCESS = "code";
    private static final String TAG_MESSAGE = "errors";
    private static final String TAG_USER = "users";
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_login);
		
		email = (EditText)findViewById(R.id.ed_username);
		password = (EditText)findViewById(R.id.ed_mailid);
		forgot_pass = (TextView)findViewById(R.id.txt_forgot_pass);
		
		password.setTypeface(Typeface.DEFAULT);
		
		prefLogin = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
		
		if(!prefLogin.getString(EMAIL, "").equalsIgnoreCase("")&&!prefLogin.getString(PWD, "").equalsIgnoreCase("")&&!prefLogin.getString(UNAME, "").equalsIgnoreCase("")){
			
			storedUsrnm = prefLogin.getString(UNAME, "");
			
			Intent intent = new Intent(LoginActivity.this, CategoriesActivity.class);
			intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
			startActivity(intent);
			finish();
			overridePendingTransition(R.anim.right_in, R.anim.left_out);
		}
			
		login = (Button)findViewById(R.id.bt_register);
		register = (TextView)findViewById(R.id.txt_register);
				
		email.setOnFocusChangeListener(new OnFocusChangeListener() {

			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				if (!hasFocus) {
					String regex = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
					Pattern pattern = Pattern.compile(regex);
					Matcher matcher = pattern.matcher(email.getText()
							.toString());
					if (email.getText().length() >= 6 && matcher.matches()) {
						email.setError(null);
					} else {
						email.setError("Invalid Email id");
						/*Toast.makeText(getApplicationContext(),
								"Invalid Email id", 1000).show();*/
					}
				}
			}
		});
		
		password.setOnFocusChangeListener(new OnFocusChangeListener() {

			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				if (!hasFocus) {
					
					if (password.getText().length()>=6) {
						password.setError(null);
					} else if(password.getText().length()>15) {
						password.setError("Password should be maximum 15 characters");
					}else{
						password.setError("Password should be minimum 6 characters");
					}
				}
			}
		});
		
		register.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(LoginActivity.this, RegisterActivity.class);
				intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
				startActivity(intent);
				overridePendingTransition(R.anim.right_in, R.anim.left_out);
			}
		});
		
		forgot_pass.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(LoginActivity.this, ForgotPasswordSendMail.class);
				startActivity(intent);
			}
		});
		
		login.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				
				if(loginValidation()){
					
					if(CategoriesActivity.isInternetConnected(LoginActivity.this)){
					new AttemptLogin().execute();
					}else{
										
						NoInternet.internetConnectionFinder(LoginActivity.this);

					}
				
				}else{
					showToast("Problem Logging In");
				}
			}
		});
	}

	class AttemptLogin extends AsyncTask<String, String, String> {

		 /**
        * Before starting background thread Show Progress Dialog
        * */
		boolean failure = false;

       @Override
       protected void onPreExecute() {
           super.onPreExecute();
           pDialog = new ProgressDialog(LoginActivity.this);
           pDialog.setMessage("Attempting login...");
           pDialog.setIndeterminate(false);
           pDialog.setCancelable(false);
           pDialog.show();
       }

		@Override
		protected String doInBackground(String... args) {
			// TODO Auto-generated method stub
			 // Check for success tag
           int success;
           
               
           String mail = email.getText().toString();
           String pword = password.getText().toString();
           
           /*gcm = GoogleCloudMessaging.getInstance(LoginActivity.this);
           if (gcm == null) {
				gcm = GoogleCloudMessaging.getInstance(context);
			}
           String gcmRegId="";*/
		/*try {
			//gcmRegId = gcm.register(Config.GOOGLE_PROJECT_ID);
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}*/
           
           
         //  String gcm1 = gcmRegId;
                 
           
           try {
               // Building Parameters
               List<NameValuePair> params = new ArrayList<NameValuePair>();
               params.add(new BasicNameValuePair("usernm", mail));
               params.add(new BasicNameValuePair("pwd", pword));
              // params.add(new BasicNameValuePair("reg_id", gcm1));

               Log.d("request!", "starting");
               // getting product details by making HTTP request
               ServiceHandler sh = new ServiceHandler();
               
               String json = sh.makeServiceCall(LOGIN_URL, 2, params);

               JSONObject jsonObj = new JSONObject(json);
               
               // check your log for json response
               //Log.d("Login attempt", json.toString());

               //JSONArray jar = json.getJSONArray("errors");
               
               // json success tag
               success = Integer.parseInt(jsonObj.getString("code"));
               if (success == 200) {
               	Log.d("Login Successful!", "  " );
               	
               
               	
               	String userdetails = jsonObj.getString(TAG_USER);
               	
               	JSONObject userJSON = new JSONObject(userdetails);
               	               	
               	user_name = userJSON.getString("u_fnm");
               
            	prefLogin=getSharedPreferences(MyPREFERENCES,Context.MODE_PRIVATE);
	           	 Editor editor = prefLogin.edit();
	           	 editor.clear();
	             editor.putString(EMAIL,mail);
	             editor.putString(PWD,pword);
	             editor.putString(UNAME, user_name);
	             editor.commit();
	             storedUsrnm = prefLogin.getString(UNAME, "");
               	Intent i = new Intent(LoginActivity.this, CategoriesActivity.class);
               	i.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
				startActivity(i);
				finish();
				overridePendingTransition(R.anim.right_in, R.anim.left_out);
               	return jsonObj.getString("code");
               }else{
            	   
               	Log.d("Login Failure!", jsonObj.getString("errors"));
               	return null;
            	   }
               	

               
           } catch (JSONException e) {
               e.printStackTrace();
           }

           return null;

		}
		/**
        * After completing background task Dismiss the progress dialog
        * **/
       protected void onPostExecute(String file_url) {
           // dismiss the dialog once product deleted
           pDialog.dismiss();
           if (file_url != null){
           	
           }else{
        	   Toast.makeText(LoginActivity.this, "Incorrect Username/Password", Toast.LENGTH_LONG).show();
           }

       }

	}
	
	public boolean loginValidation()
	{
			Log.e("Login Validation", "msg");	
			            
			if (email.getText().toString().equalsIgnoreCase("") || password.getText().toString().equalsIgnoreCase("") || password.getText().toString().length()<6
					|| password.getText().toString().length()>15)
			{
				if(email.getText().toString().trim().equalsIgnoreCase(""))
				{
					email.setError("Username should be filled");
				}
				if(password.getText().toString().trim().equalsIgnoreCase(""))
				{
					password.setError("Password should be filled");
				}
				if(password.getText().toString().length()<6){
					password.setError("Password should be minimum 6 characters long");
				}
				if(password.getText().toString().length()>15){
					password.setError("Password should not exceed 15 characters");
				}
				return false;
			}
			else
			{
				email.setError(null);
				password.setError(null);
				return true;
				
			}
		
	}
	
	private void showToast(String msg)
	{
		Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_SHORT).show();
	}
		
}
